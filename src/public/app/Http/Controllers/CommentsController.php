<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Comment;
use App\User;

class CommentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()->role == 'default' || Auth::user()->role == 'redactor'){
            $comments = Comment::where('user_id', '=', Auth::user()->id)->get();
            $answers = collect();
            foreach($comments as $comment){
                foreach($comment->answers as $answer){
                    $answers->push($answer);
                }
            }
			
            //dd(Auth::user()->Comments[0]);

            if (count($comments) > 0){
                $ThisComment = Auth::user()->Comments[0];

                $comments = $comments->merge($answers);
                $comments = $comments->sortBy('created_at');

                return view('pages.comments', ['comments' => $comments, 'ThisComment' => $ThisComment]);
            }
            else{
                $comments = $comments->merge($answers);
                $comments = $comments->sortBy('created_at');

                return view('pages.comments', ['comments' => $comments]);
            }
        }
        else if (Auth::user()->role == 'comitet'){
            $comments = collect();
            $users = User::where('role', '<>', 'comitet')->get();

            foreach($users as $user){
                foreach($user->Comments as $comment){
                    $comments->push($comment);
                }
            }

            return view('pages.commentsComitet', ['comments' => $comments]);
        }
    }
	

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'text' => 'required|max:255',
		]);
		
		$comment = new Comment;
		
        $comment->text = $request->text;
		$comment->user_id = $request->user_id;
		
		$comment->save();
		session()->flash('message', 'Новый комментарий успешно добавлен!');
		
		return redirect()->route('comments.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
