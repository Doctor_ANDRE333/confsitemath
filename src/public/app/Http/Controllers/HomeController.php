<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;
use App\Comment;
use App\User;
use App\ConfNew;
use App\Report;
use App\Answer;
use App\Diplom;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
   //     $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexHome(){
        return view('pages.welcome');
    }
	
    public function indexContacts(){
        return view('pages.contacts');
    }

    public function indexPhotos(){
        return view('pages.photos');
    }

    public function indexNews(){
        $news = ConfNew::all();
        return view('pages.news', ['news' => $news]);
    }
	
	public function indexComitet(){
       return view('pages.comitet');
    }
	
	public function indexArchive(){
        return view('pages.archive');
    }

    public function indexRequirements(){
        return view('pages.requirements');
    }

    public function indexConf(){
        return view('pages.aboutconf');
    }
	
	public function indexUpload(){
        return view('pages.uploadWork');
    }
	
    public function AboutMe(){
        if (Auth::user()){
			$user = Auth::user();
            return view('pages.AboutMe', ['user'=>$user]);
        } else {
            return view('auth.login');
        }
    }

    public function WorkStatus()
	{	
		if (Auth::user()->role == 'default')
		{
			$reports = Auth::user()->Report;
			return view('pages.WorkStatus', ['reports'=>$reports]);
		}
		else
		{
			$reports = Report::All();
			return view('pages.WorkStatusComitet', ['reports'=>$reports]);
		}	
    }
	
	public function updateMe(Request $request, $id)
    {
		$this->validate($request, [
			'name' => 'required|max:255',
			'email' => 'required|email|max:255',
            'birth' => 'required'
		]);

        if (count(User::where('email', '=', $request->email)->where('id', '<>', $id)->get()) != 0){ 
            session()->flash('message', 'Данный email уже используется!');
            return redirect()->route('AboutMe', $id);
        }
        else{
            $i = User::find($id);
            
            $i->name = $request->name;
            $i->surname = $request->surname;
			$i->group = $request->group;
            $i->birth = $request->birth;
            $i->email = $request->email;

            if ($request->passCheck == 1){
                $this->validate($request, [
					'password' => 'required|min:6|confirmed'
				]);
                
                $i->password = bcrypt($request->password);
            }
	/*   
    */
            $i->save();
            session()->flash('message', 'Личные данные успешно обновлены!');
            return redirect('/lk/AboutMe');
        }
    }
	
	public function indexComitetComments($id){
        if (Auth::user()->role == 'comitet'){
           $comment = Comment::find($id);
           $ThisComment = $comment;
           $comments = collect();
           $comments = $comment->Answers;
           $comments->push($comment);
           $comments = $comments->sortBy('created_at');
           return view('pages.comments', ['comments' => $comments, 'ThisComment' => $ThisComment]);
        }
    }
	
	public function indexDiploms(){
		  if (Auth::user()->role == 'comitet')
		  {
			$users = User::where('role','=','default')->get();	
			$diploms = Diplom::All();
			return view('pages.diplomsComitet', ['users' => $users, 'diploms' => $diploms]);
		  }
		  else if (Auth::user()->role == 'default')
		  {
			  return view('pages.diploms');
		  }
    }

    public function sendAnswer(Request $request, $comment_id){
        $this->validate($request, [
			'text' => 'required|max:255',
		]);
		$answer = new Answer;
		
        $answer->text = $request->text;
		$answer->user_id = $request->user_id;
        $answer->comment_id = $comment_id;
		
		$answer->save();
		session()->flash('message', 'Новый ответ успешно добавлен!');
		
		return redirect()->route('comments.index');
    }
	
	
    public function editStatus(Request $request, $id){
        $report = Report::find($id);

        //$arr = ['На рассмотрении', 'Работа одобрена', 'Работа отклонена'];
		//unset($arr[array_search("$report->status", $arr)]);
		
        $report->status = $request->status;
		$report->save();
		return redirect()->route('WorkStatus');
    }
	
	public function getFile($id){
		$report = Report::find($id);
        $filename = $report->User->surname . "_" . $report->User->name . "_" . $report->User->group . ".docx";
	//	  dd($filename);
		$location = public_path() . '/Reports/' . $filename;

		return response()->download($location, $filename);
    }
	
	public function makeDiplom(Request $request)  
    {  
		$user = User::find($request->user);
		$write = $user->name . " " . $user->surname . "\n" . $request->nomination;
       $img = Image::make(public_path('example.jpg'));  
       $img->text($write, 120, 100, function($font) {  
          $font->file(public_path('path/font.ttf'));  
          $font->size(28);  
          $font->color('#e1e1e1');  
          $font->align('center');  
          $font->valign('bottom');  
          $font->angle(90);  
      });  
       $img->save(public_path('diplom.jpg'));  
    }
}
