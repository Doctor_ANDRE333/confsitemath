<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

use App\Report;

class WorkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		if ((empty(Auth::user()->surname)) || (empty(Auth::user()->surname)) || (empty(Auth::user()->group))){
			session()->flash('error', 'Сначала заполните информацию о себе!');
			return redirect('/lk/WorkStatus');
		}
		else{
			$user = Auth::user();
            return view('pages.createWork', ['user'=>$user]);
		}
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		 $this->validate($request, [
			'label' => 'required|min:1|max:50',
			'topic' => 'required|min:1|max:50',
			'group' => 'required|min:1|max:20',
		]);
		
        if($request->file('file')){
                    
            $report = new Report;
            $report->user_id = Auth::user()->id;
            $report->label = $request->label;
            $report->topic = $request->topic;
            $report->group = $request->group;
            $report->collaborator = $request->collaborator;

            $file = $request->file('file');
            $filename = Auth::user()->surname . "_" . Auth::user()->name . "_" . Auth::user()->group . ".docx";
            $location = public_path() . '/Reports';
            $file->move($location,$filename);
            $report->filename = $filename; 
            $report->save();
            
            session()->flash('message', 'Работа успешно загружена!');
        }
        else{
            session()->flash('error', 'Возникли проблемы с загрузкой файла :/');
        }

		return redirect('/lk/WorkStatus');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		$report = Report::find($id);
        return view('pages.updateWork', ['report' => $report]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'label' => 'required|min:1|max:50',
			'topic' => 'required|min:1|max:50',
			'group' => 'required|min:1|max:20',
		]);
		
		if($request->file('file')) {
			$report = Report::find($id);
			$report->label = $request->label;
			$report->topic = $request->topic;
			$report->group = $request->group;
			$report->collaborator = $request->collaborator;

            $file = $request->file('file');
            $filename = Auth::user()->surname . "_" . Auth::user()->name . "_" . Auth::user()->group . ".docx";
            $location = public_path() . '/Reports';
            $file->move($location,$filename);
			$report->filename = $filename; 
			$report->save();
			
			session()->flash('message', 'Работа успешно загружена!');
        }
		else
		{
			session()->flash('message', 'Возникли проблемы с загрузкой файла :/');
		}

		return redirect('/lk/WorkStatus');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
