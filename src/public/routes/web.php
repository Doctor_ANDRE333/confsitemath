<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/


Auth::routes();

Route::get('/', 'HomeController@indexHome')->name('Welcome');
Route::get('/contacts', 'HomeController@indexContacts')->name('Contacts');
Route::get('/confNews', 'HomeController@indexNews')->name('ConfNews');
Route::get('/photos', 'HomeController@indexPhotos')->name('Photos');
Route::get('/comitet', 'HomeController@indexComitet')->name('Comitet');
Route::get('/archive', 'HomeController@indexArchive')->name('Archive');
Route::get('/requirements', 'HomeController@indexRequirements')->name('Requirements');
Route::get('/aboutconf', 'HomeController@indexConf')->name('aboutConf');


Route::get('/lk/AboutMe', 'HomeController@AboutMe')->name('Info');
Route::get('/lk/WorkStatus', 'HomeController@WorkStatus')->name('WorkStatus');
Route::get('/lk/Diploms', 'HomeController@indexDiploms')->name('Diploms');
//Route::get('/lk/comments', 'HomeController@getComments')->name('Comments');

//Route::get('/lk/UploadWork', 'HomeController@indexUpload')->name('UploadWork');


Route::post('/lk/update/{id}', 'HomeController@UpdateMe')->name('UpdateMe');
Route::post('/lk/answer/{id}', 'HomeController@sendAnswer')->name('SendAnswer');
Route::post('/lk/editStatus/{id}', 'HomeController@editStatus')->name('editStatus');
Route::post('/lk/makeDiplom', 'HomeController@makeDiplom')->name('makeDiplom');
Route::get('/lk/getFile/{id}', 'HomeController@getFile')->name('getFile');
Route::get('lk/commentsComitet/{id}', 'HomeController@indexComitetComments')->name('indexComitetComments');

Route::resource('/news', 'NewController');
Route::resource('/works', 'WorkController');
Route::resource('/lk/comments', 'CommentsController');
