@extends('layouts.app1')

@section('title')
	Главная страница
@endsection

@section('content')

    <div class = "container rounded" style = "margin-top: 100px; background-color: WhiteSmoke">
        <div class = "row">
            <div class = "col-12">
                <h1 style = "font-family: Times New Roman, Georgia, Serif;"> Название конференции </h1>
            </div>
        </div>

        <div class = "row mt-2">
            <div class = "col-md-6 col-12">
                <p class = "text-justify" style="font-family: Gilroy-Heavy;" > 
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Cum rerum eaque quam quas molestiae molestias, quo eos laboriosam labore minima consequuntur. Molestias minima commodi maiores, ea nam iure? Perspiciatis, veritatis.
                    Lorem ipsum, dolor sit amet consectetur adipisicing elit. Aspernatur, debitis aut! Harum corrupti error laborum id atque, quisquam vero at totam neque nobis. Adipisci enim rerum quos iusto! Ea, nostrum!
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Consequuntur neque voluptate minus aut, accusamus animi veniam provident quas omnis repudiandae laudantium amet facilis illum numquam quo eligendi tempora quod modi.
                    Lorem ipsum dolor, sit amet consectetur adipisicing elit. Tempore minus atque iste rem, quod doloremque earum ad rerum quis adipisci qui consequuntur, id molestiae, dolorem hic asperiores possimus officiis ea.
                    Lorem ipsum, dolor sit amet consectetur adipisicing elit. Corporis, accusamus. Dolorum corporis minus qui atque hic quo accusamus ad, eaque quasi, illo est eum at provident. Magni molestias odio iusto.
                </p>
            </div>

            <div class = "col-md-6 col-12">
                <img class = "mt-2 p-1 rounded mb-1" src = "http://pictures.std-1056.ist.mospolytech.ru/conf.jpg" width = "100%" style = "border: 2px solid rgba(135, 75, 160);">
            </div>
        </div>
    </div>

    <div class = "container-fluid mt-4 pt-4" style="background-color: whitesmoke; min-height: 430px">
        <div class = "row">
			
            <div class = "col-md-4 col-12 mb-2">
                <div class="card p-2">
                    <a href = "/requirements"><img class="card-img-top hover-zoom" src="http://pictures.std-1056.ist.mospolytech.ru/p2.jpg" alt="Card image cap">

                    <div class="card-body">
                        <h5 class = "text-center">
                            <a> Требования к статьям </a>
                        </h5>
                    </div>
                </div>
            </div>
			
            <div class = "col-md-4 col-12 mb-2">
                <div class="card p-2">
                    <a href="/photos"> <img class="card-img-top hover-zoom" src="http://pictures.std-1056.ist.mospolytech.ru/p1.jpg" alt="Card image cap"> </a>

                    <div class="card-body">
                        <h5 class = "text-center">
                            <a> Посмотреть фотоархив </a>
                        </h5>
                    </div>
                </div>
            </div>

            <div class = "col-md-4 col-12 mb-3">
                <div class="card p-2">
                    <a href="/aboutconf"> <img class="card-img-top hover-zoom" src="http://pictures.std-1056.ist.mospolytech.ru/p3.jpg" alt="Card image cap"> </a>

                    <div class="card-body">
                        <h5 class = "text-center">
                            <a> Информация о конференции</a>
                        </h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
<style>
    body{
        background: url(http://pictures.std-1056.ist.mospolytech.ru/bg.jpg);
    }
</style>

@endsection