@extends('layouts.AppLK')

@section('title')
	Редактирование работы
@endsection

@section('content')
	<div class = "container" style = "min-height: 700px; margin-top: 100px">
			<div class="text-center ml-5 mr-5">
			@include('common.errors')
			<div>
        <div class="card text-dark bg-light mt-5" style = "width: 75%; margin: 0 auto">
            <div class="card-header">
                <h5 class = "text-center"> Редактирование работы </h5>
            </div>

            <div class="card-body"  id="cardW">

                <form action="{{ route('works.update', $report->id) }}" method="POST" class="form-horizontal mt-3" enctype="multipart/form-data">
                    {{ csrf_field() }}
					{{ method_field('PUT') }}
                    <div class = "row">
						<div class = "col-12">
                            <b> Автор: </b>
							<h5><p> {{ $report->User->surname }}&nbsp;{{ $report->User->name }} </p></h5>
                        </div>
                        <div class = "col-12 mt-2">
                            <b> Тема: </b> 
                            <input type = "text" value="{{ $report->topic }}" name = "topic" class = "form-control">
                        </div>

                        <div class = "col-12 mt-2">
                            <b> Название работы: </b>
                            <input type = "text" value="{{ $report->label }}"  name = "label" class = "form-control">
                        </div>
						<div class = "col-12 mt-2">
                            <b> Группа: </b>
                             <input type = "text" value="{{ $report->group }}" name = "group" class = "form-control">
                        </div>
						<div class = "col-12 mt-2">
                            <b> Соавторы: </b>
                             <input type = "text" value="{{ $report->collaborator }}" name = "collaborator"  class = "form-control">
                        </div>
						<div class = "col-12 mt-2">
							<b> Документ (загружайте только в формате .docx): </b>
							<input type="file"  name="file" class="form-control mb-3 ">
						</div>
                    </div>
                            <input type = "submit" name = "Sub" value = "Редактировать работу" class = "form-control mt-3 btn btn-primary" >
                </form>
            </div>
        </div>
	</div>
	
	<style>
		body{
        background-image: url(http://pictures.std-1056.ist.mospolytech.ru/blackfon.jpg)
    }

	#cardW{
		background-image: url(http://pictures.std-1056.ist.mospolytech.ru/whitefon.jpg)
	}
	</style>

@endsection