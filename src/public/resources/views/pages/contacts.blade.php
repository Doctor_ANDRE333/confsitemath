@extends('layouts.app1')

@section('title')
	Контакты
@endsection

@section('content')
    <div class="container" style = "margin-top: 80px; text-align: center; min-height: 650px">
        <div class="card text-dark" style = "margin-top: 100px">
            <div class="card-header">
                <h1> Контакты </h1>
            </div>
            <div class="card-body">
                <div class="intro">
                    <h4 class = "mb-4"> ПН-ПТ 09:30–18:30 </h4>
                </div>
                   
                <div class = "text-left lBord">
                    <div class = "ml-4">
                        <p> Заведующий кафедрой: Андреев Степан Николаевич</p>
                        
                        <p>Телефон: <a href="tel:+74952230523" property="telephone">+7(495)223-05-23</a> доб.3140.</p>
                        <p>Делопроизводитель: Свиридова Анна Владимировна</p>
                        
                        <p>Телефон: <a href="tel:+74952230523" property="telephone">+7(495)223-05-23</a> доб.3140.</p>
                        
                        <p> E-mail: km@mospolytech.ru</p>
                        
                        <p>Адрес: г.Москва, ул.Павла Корчагина, д.22, ауд. ПК441.</p>
                    </div>
                </div>
            </div>
        </div>  
    </div>
    
<style>
    .lBord{
        border-left: 2px solid rgba(135, 75,160)
    }

    body{
          background-image: url(http://pictures.std-1056.ist.mospolytech.ru/blackfon.jpg)
    }

    .card{
        border: 1px solid black;
		background-image: url(http://pictures.std-1056.ist.mospolytech.ru/whitefon.jpg)
    }
</style>
@endsection