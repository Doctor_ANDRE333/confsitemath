@extends('layouts.app1')

@section('title')
	Требования к статьям
@endsection

@section('content')

<style>
    body{
        background-image: url(http://pictures.std-1056.ist.mospolytech.ru/blackfon.jpg)
    }
	
	.teacher{
		color: white
	}

	h3{
        padding: 10px;
		color:  rgba(135, 75,160)
    }

	.maininfo{
		background-image: url(http://pictures.std-1056.ist.mospolytech.ru/whitefon.jpg)
	}
</style>

<div class="container-fluid mb-3">
    <div class="row">
        <div class = "col-12">
            <h1 class = "text-center" style="margin-top: 100px; background-image: url(https://wallup.net/wp-content/uploads/2017/11/23/433673-minimalism-low_poly.jpg); border-radius: 20px"> Требования к статьям на конференцию </h1>
        </div>
    </div>
</div>

<div class = "container rounded maininfo mb-12">
    <div class = "row">
        <div class = "col-12">
            <h1 class="text-center" style = "font-family: Times New Roman, Georgia, Serif;"> Основные требования </h1>
        </div>
    </div>

    <div class = "row mt-12">
        <div class = "col-md-12 col-12">
        <h3 style = "font-family: Times New Roman, Georgia, Serif;"> <strong> 1. Статьи должны быть... </strong> </h3>
            <p class = "text-justify"> 
                Lorem ipsum dolor sit. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dicta voluptatum porro atque numquam vitae perspiciatis facilis quod, sunt dignissimos similique nam facere doloremque sint non.
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Provident dolor quia alias officia corporis, voluptates dolore laudantium labore error. Suscipit eveniet accusamus deleniti ut iusto dolore itaque amet. Nesciunt, alias!
            </p>
        </div>

        <div class = "col-md-12 col-12">
        <h3 style = "font-family: Times New Roman, Georgia, Serif;"> <strong> 2. Статьи должны быть... </strong> </h3>
            <p class = "text-justify"> 
                Lorem ipsum dolor sit. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dicta voluptatum porro atque numquam vitae perspiciatis facilis quod, sunt dignissimos similique nam facere doloremque sint non.
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Provident dolor quia alias officia corporis, voluptates dolore laudantium labore error. Suscipit eveniet accusamus deleniti ut iusto dolore itaque amet. Nesciunt, alias!
            </p>
        </div>

        <div class = "col-md-12 col-12">
        <h3 style = "font-family: Times New Roman, Georgia, Serif;"> <strong> 3. Статьи должны быть... </strong> </h3>
            <p class = "text-justify"> 
                Lorem ipsum dolor sit. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dicta voluptatum porro atque numquam vitae perspiciatis facilis quod, sunt dignissimos similique nam facere doloremque sint non.
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Provident dolor quia alias officia corporis, voluptates dolore laudantium labore error. Suscipit eveniet accusamus deleniti ut iusto dolore itaque amet. Nesciunt, alias!
            </p>
        </div>

        <div class = "col-md-12 col-12">
        <h3 style = "font-family: Times New Roman, Georgia, Serif;"> <strong> 4. Статьи должны быть... </strong> </h3>
            <p class = "text-justify"> 
                Lorem ipsum dolor sit. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dicta voluptatum porro atque numquam vitae perspiciatis facilis quod, sunt dignissimos similique nam facere doloremque sint non.
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Provident dolor quia alias officia corporis, voluptates dolore laudantium labore error. Suscipit eveniet accusamus deleniti ut iusto dolore itaque amet. Nesciunt, alias!
            </p>
        </div>

        <div class = "col-md-12 col-12">
            <h5 class = "text-center">
                <a href="http://pictures.std-1056.ist.mospolytech.ru/trebovania.docx" download="Требования к статьям"><strong>Скачать требования в формате .docx</strong></a>
            </h5>
        </div>
    </div>
</div>

<br>
<br>