@extends('layouts.AppLK')

@section('title')
	Дипломы и сертификаты
@endsection

@section('content')

<main class="page-content">
    <div class="container fon">
		@include('common.errors')
		
		@if (Session::has('message'))
			<div class = "alert alert-primary mt-3"> {{ Session::get('message') }} </div>
		@endif
		
		<form action = "{{ route('makeDiplom') }}" method = "POST">
		 {{ csrf_field() }}
		<div class="row">
			<div class="col-6">
				<h3>Участник </h3>
				<select name="user" class="form-control">
					@foreach ($users as $user)
					<option  value="{{ $user->id }}">{{ $user->name . " " . $user->surname }}</option>
					@endforeach
				</select>
			</div>
			<div class="col-6">
				<h3>Номинация</h3>
				<select name="nomination" class="form-control">
					<option  value="winner">Победителю</option>
					<option  value="prizer">Призёру</option>
					<option  value="participant">Участнику</option>
				</select>
			
			</div>
			<div class="col-12 mt-2">
				<button type="submit" class="form-control btn-secondary"> Создать</button>
			</div>
		
		</div>
		</form>
	<div class="row mt-5 text-center">
		<div class="col-12">
			<h3>Созданные сертификаты и дипломы</h3>
			
			<table class="table table-hover table-dark">
			  <thead>
				<tr>
				  <th scope="col">Данные</th>
				  <th scope="col">Номинация</th>
				  <th scope="col">Скачать файл</th>
				</tr>
			  </thead>
			  <tbody>
			  @foreach ($diploms as $diplom) 
				<tr>
				  <td>{{$diplom->User->name . " " . $diplom->User->surname . " " . $diplom->User->group }}</td>
				  <td>{{$diplom->nomination}}</td>
				  <td><a href="">{{$diplom->filename}}</a></td>
				</tr>
			@endforeach
			  </tbody>
			</table>
		</div>
	</div>

    </div>
</main>

<style>
    body{
        background-image: url(http://pictures.std-1056.ist.mospolytech.ru/blackfon.jpg)
    }
	
	.fon{
		background-image: url(http://pictures.std-1056.ist.mospolytech.ru/whitefon.jpg);
		border-radius: 5px;
	}
</style>

@endsection