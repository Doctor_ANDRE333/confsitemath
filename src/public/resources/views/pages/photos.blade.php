@extends('layouts.app1')

@section('title')
	Фотоархив
@endsection

@section('content')

<!-- fancyBox CSS -->
<link href="http://pictures.std-1056.ist.mospolytech.ru/jquery.fancybox.min.css" rel="stylesheet">
<link href="http://pictures.std-1056.ist.mospolytech.ru/jquery.fancybox.css" rel="stylesheet">

    <style>
    .thumb img {
        -webkit-filter: grayscale(0);
        filter: none;
        border-radius: 5px;
        background-color: #fff;
        border: 1px solid #ddd;
		transition: all 1s;

        
        padding: 5px;
    }

    .thumb img:hover {
        -webkit-filter: grayscale(1);
        filter: grayscale(1);
    }

    .thumb {
        padding: 5px;
    }

    body{
        background-image: url(http://70b0847f967d1d37694c-789248f0448453b835406008213ad4a2.r86.cf2.rackcdn.com/cover_photo_311239_1464547715.jpg)
    }
</style>

<div class="container-fluid mb-3">
    <div class="row">
        <div class = "col-12">
            <h1 class = "text-center" style="margin-top: 100px; background-image: url(https://wallup.net/wp-content/uploads/2017/11/23/433673-minimalism-low_poly.jpg); border-radius: 20px"> Фотоархив конференции </h1>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-lg-3 col-md-4 col-6 thumb">
            <a data-fancybox="gallery" href="https://liquipedia.net/commons/images/thumb/e/ef/Bondik_at_ESL_One_Katowice_2015_LAN_Qualifier.jpg/450px-Bondik_at_ESL_One_Katowice_2015_LAN_Qualifier.jpg">
                <img class="img-fluid" src="https://liquipedia.net/commons/images/thumb/e/ef/Bondik_at_ESL_One_Katowice_2015_LAN_Qualifier.jpg/450px-Bondik_at_ESL_One_Katowice_2015_LAN_Qualifier.jpg" alt="">
            </a>
        </div>
        <div class="col-lg-3 col-md-4 col-6 thumb">
            <a data-fancybox="gallery" href="https://liquipedia.net/commons/images/thumb/c/ca/Markeloff_at_PGL_Major_Krakow_2017.jpg/600px-Markeloff_at_PGL_Major_Krakow_2017.jpg">
                <img class="img-fluid" src="https://liquipedia.net/commons/images/thumb/c/ca/Markeloff_at_PGL_Major_Krakow_2017.jpg/600px-Markeloff_at_PGL_Major_Krakow_2017.jpg" alt="">
            </a>
        </div>
        <div class="col-lg-3 col-md-4 col-6 thumb">
            <a data-fancybox="gallery" href="https://img.championat.com/s/735x490/news/big/c/u/georgij-worldedit-jaskin-hochetsja-vyigrat-bolshoj-turnir_14738678401218961104.jpg">
                <img class="img-fluid" src="https://img.championat.com/s/735x490/news/big/c/u/georgij-worldedit-jaskin-hochetsja-vyigrat-bolshoj-turnir_14738678401218961104.jpg" alt="">
            </a>
        </div>
        <div class="col-lg-3 col-md-4 col-6 thumb">
            <a data-fancybox="gallery" href="https://s-cdn.sportbox.ru/images/styles/upload/fp_fotos/2b/d8/8459e896f2156cab8bad96b21fcce84a5ddd819c2be61626847630.jpg">
                <img class="img-fluid" src="https://s-cdn.sportbox.ru/images/styles/upload/fp_fotos/2b/d8/8459e896f2156cab8bad96b21fcce84a5ddd819c2be61626847630.jpg" alt="">
            </a>
        </div>
        <div class="col-lg-3 col-md-4 col-6 thumb">
            <a data-fancybox="gallery" href="https://s-cdn.sportbox.ru/images/styles/upload/fp_fotos/2b/d8/8459e896f2156cab8bad96b21fcce84a5ddd819c2be61626847630.jpg">
                <img class="img-fluid" src="https://s-cdn.sportbox.ru/images/styles/upload/fp_fotos/2b/d8/8459e896f2156cab8bad96b21fcce84a5ddd819c2be61626847630.jpg" alt="">
            </a>
        </div>
        <div class="col-lg-3 col-md-4 col-6 thumb">
            <a data-fancybox="gallery" href="https://s-cdn.sportbox.ru/images/styles/upload/fp_fotos/2b/d8/8459e896f2156cab8bad96b21fcce84a5ddd819c2be61626847630.jpg">
                <img class="img-fluid" src="https://s-cdn.sportbox.ru/images/styles/upload/fp_fotos/2b/d8/8459e896f2156cab8bad96b21fcce84a5ddd819c2be61626847630.jpg" alt="">
            </a>
        </div>
        <div class="col-lg-3 col-md-4 col-6 thumb">
            <a data-fancybox="gallery" href="https://s-cdn.sportbox.ru/images/styles/upload/fp_fotos/2b/d8/8459e896f2156cab8bad96b21fcce84a5ddd819c2be61626847630.jpg">
                <img class="img-fluid" src="https://s-cdn.sportbox.ru/images/styles/upload/fp_fotos/2b/d8/8459e896f2156cab8bad96b21fcce84a5ddd819c2be61626847630.jpg" alt="">
            </a>
        </div>
        <div class="col-lg-3 col-md-4 col-6 thumb">
            <a data-fancybox="gallery" href="https://s-cdn.sportbox.ru/images/styles/upload/fp_fotos/2b/d8/8459e896f2156cab8bad96b21fcce84a5ddd819c2be61626847630.jpg">
                <img class="img-fluid" src="https://s-cdn.sportbox.ru/images/styles/upload/fp_fotos/2b/d8/8459e896f2156cab8bad96b21fcce84a5ddd819c2be61626847630.jpg" alt="">
            </a>
        </div>
        <div class="col-lg-3 col-md-4 col-6 thumb">
            <a data-fancybox="gallery" href="https://s-cdn.sportbox.ru/images/styles/upload/fp_fotos/2b/d8/8459e896f2156cab8bad96b21fcce84a5ddd819c2be61626847630.jpg">
                <img class="img-fluid" src="https://s-cdn.sportbox.ru/images/styles/upload/fp_fotos/2b/d8/8459e896f2156cab8bad96b21fcce84a5ddd819c2be61626847630.jpg" alt="">
            </a>
        </div>
        <div class="col-lg-3 col-md-4 col-6 thumb">
            <a data-fancybox="gallery" href="https://s-cdn.sportbox.ru/images/styles/upload/fp_fotos/2b/d8/8459e896f2156cab8bad96b21fcce84a5ddd819c2be61626847630.jpg">
                <img class="img-fluid" src="https://s-cdn.sportbox.ru/images/styles/upload/fp_fotos/2b/d8/8459e896f2156cab8bad96b21fcce84a5ddd819c2be61626847630.jpg" alt="">
            </a>
        </div>
        <div class="col-lg-3 col-md-4 col-6 thumb">
            <a data-fancybox="gallery" href="https://s-cdn.sportbox.ru/images/styles/upload/fp_fotos/2b/d8/8459e896f2156cab8bad96b21fcce84a5ddd819c2be61626847630.jpg">
                <img class="img-fluid" src="https://s-cdn.sportbox.ru/images/styles/upload/fp_fotos/2b/d8/8459e896f2156cab8bad96b21fcce84a5ddd819c2be61626847630.jpg" alt="">
            </a>
        </div>
        <div class="col-lg-3 col-md-4 col-6 thumb">
            <a data-fancybox="gallery" href="https://s-cdn.sportbox.ru/images/styles/upload/fp_fotos/2b/d8/8459e896f2156cab8bad96b21fcce84a5ddd819c2be61626847630.jpg">
                <img class="img-fluid" src="https://s-cdn.sportbox.ru/images/styles/upload/fp_fotos/2b/d8/8459e896f2156cab8bad96b21fcce84a5ddd819c2be61626847630.jpg" alt="">
            </a>
        </div>
        <div class="col-lg-3 col-md-4 col-6 thumb">
            <a data-fancybox="gallery" href="https://s-cdn.sportbox.ru/images/styles/upload/fp_fotos/2b/d8/8459e896f2156cab8bad96b21fcce84a5ddd819c2be61626847630.jpg">
                <img class="img-fluid" src="https://s-cdn.sportbox.ru/images/styles/upload/fp_fotos/2b/d8/8459e896f2156cab8bad96b21fcce84a5ddd819c2be61626847630.jpg" alt="">
            </a>
        </div>
        <div class="col-lg-3 col-md-4 col-6 thumb">
            <a data-fancybox="gallery" href="https://s-cdn.sportbox.ru/images/styles/upload/fp_fotos/2b/d8/8459e896f2156cab8bad96b21fcce84a5ddd819c2be61626847630.jpg">
                <img class="img-fluid" src="https://s-cdn.sportbox.ru/images/styles/upload/fp_fotos/2b/d8/8459e896f2156cab8bad96b21fcce84a5ddd819c2be61626847630.jpg" alt="">
            </a>
        </div>
        <div class="col-lg-3 col-md-4 col-6 thumb">
            <a data-fancybox="gallery" href="https://s-cdn.sportbox.ru/images/styles/upload/fp_fotos/2b/d8/8459e896f2156cab8bad96b21fcce84a5ddd819c2be61626847630.jpg">
                <img class="img-fluid" src="https://s-cdn.sportbox.ru/images/styles/upload/fp_fotos/2b/d8/8459e896f2156cab8bad96b21fcce84a5ddd819c2be61626847630.jpg" alt="">
            </a>
        </div>
        <div class="col-lg-3 col-md-4 col-6 thumb">
            <a data-fancybox="gallery" href="https://s-cdn.sportbox.ru/images/styles/upload/fp_fotos/2b/d8/8459e896f2156cab8bad96b21fcce84a5ddd819c2be61626847630.jpg">
                <img class="img-fluid" src="https://s-cdn.sportbox.ru/images/styles/upload/fp_fotos/2b/d8/8459e896f2156cab8bad96b21fcce84a5ddd819c2be61626847630.jpg" alt="">
            </a>
        </div>
        <div class="col-lg-3 col-md-4 col-6 thumb">
            <a data-fancybox="gallery" href="https://s-cdn.sportbox.ru/images/styles/upload/fp_fotos/2b/d8/8459e896f2156cab8bad96b21fcce84a5ddd819c2be61626847630.jpg">
                <img class="img-fluid" src="https://s-cdn.sportbox.ru/images/styles/upload/fp_fotos/2b/d8/8459e896f2156cab8bad96b21fcce84a5ddd819c2be61626847630.jpg" alt="">
            </a>
        </div>
        <div class="col-lg-3 col-md-4 col-6 thumb">
            <a data-fancybox="gallery" href="https://s-cdn.sportbox.ru/images/styles/upload/fp_fotos/2b/d8/8459e896f2156cab8bad96b21fcce84a5ddd819c2be61626847630.jpg">
                <img class="img-fluid" src="https://s-cdn.sportbox.ru/images/styles/upload/fp_fotos/2b/d8/8459e896f2156cab8bad96b21fcce84a5ddd819c2be61626847630.jpg" alt="">
            </a>
        </div>
    </div>
</div>

<!-- jQuery -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<!-- Popper -->
<script src="pacanizakintekudanado/popper.js"></script>
<!-- fancyBox JS -->
<script src="http://pictures.std-1056.ist.mospolytech.ru/jquery.fancybox.min.js"></script>
<script src="http://pictures.std-1056.ist.mospolytech.ru/jquery.fancybox.js"></script>
<!-- Gridify -->
<script src="http://pictures.std-1056.ist.mospolytech.ru/gridify.js"></script>
<script>
    $(function () {
        var options = {
            srcNode: 'img',             // grid items (class, node)
            margin: '15px',             // margin in pixel, default: 0px
            width: '240px',             // grid item width in pixel, default: 220px
            max_width: '',              // dynamic gird item width if specified, (pixel)
            resizable: true,            // re-layout if window resize
            transition: 'all 1s ease' // support transition for CSS3, default: all 0.5s ease
        };
        $('.grid').gridify(options);
    });
</script>

@endsection