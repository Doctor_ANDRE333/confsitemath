@extends('layouts.app1')

@section('title')
    Информация о комитете
@endsection

@section('content')

    <style>
        body {
            background-image: url(http://pictures.std-1056.ist.mospolytech.ru/blackfon.jpg)
        }

        .teacher {
            color: white
        }

    </style>

    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <h1 class="text-center"
                    style="margin-top: 100px; background-image: url(https://wallup.net/wp-content/uploads/2017/11/23/433673-minimalism-low_poly.jpg); border-radius: 20px">
                    Организационный комитет </h1>
            </div>
        </div>
    </div>
    <br>
    <br>
    <div class="container mb-5" style="max-width: 1000px;">

        <div class="row mb-4">
            <div class="col-sm-4">
                <img class="shadow-sm img-fluid"
                    src="https://old.mospolytech.ru/storage/43ec517d68b6edd3015b3edc9a11367b/images/.thumbs/700f9033483987839fa6e638787fba67_0_500_0.jpg"
                    alt="Описание изображения..." style="border: 5px solid #fff;">
                <p class="text-center teacher">Бойкова Галина Васильевна</p>
                <p class="text-center teacher">к.э.н, МПУ</p>
            </div>
            <div class="col-sm-4">
                <img class="shadow img-fluid"
                    src="https://old.mospolytech.ru/storage/43ec517d68b6edd3015b3edc9a11367b/images/.thumbs/30556866e3e5974c3d599fb88c8bc957_0_500_0.jpg"
                    alt="Описание изображения..." style="border: 5px solid #fff;">
                <p class="text-center teacher">Муханов Сергей Александрович</p>
                <p class="text-center teacher">к.п.н, МПУ</p>
            </div>
            <div class="col-sm-4">
                <img class="shadow-lg img-fluid"
                    src="https://old.mospolytech.ru/storage/43ec517d68b6edd3015b3edc9a11367b/images/.thumbs/2518a3c64f0194664973cc201a21154b_0_500_0.jpg"
                    alt="Описание изображения..." style="border: 5px solid #fff;">
                <p class="text-center teacher">Архангельский Александр Игоревич</p>
                <p class="text-center teacher">к.п.н, МПУ</p>
            </div>
        </div>

        <div class="row mb-4">
            <div class="col-sm-4">
                <img class="shadow-sm img-fluid"
                    src="https://old.mospolytech.ru/storage/43ec517d68b6edd3015b3edc9a11367b/images/.thumbs/700f9033483987839fa6e638787fba67_0_500_0.jpg"
                    alt="Описание изображения..." style="border: 5px solid #fff;">
                <p class="text-center teacher">Бойкова Галина Васильевна</p>
                <p class="text-center teacher">к.э.н, МПУ</p>
            </div>
            <div class="col-sm-4">
                <img class="shadow img-fluid"
                    src="https://old.mospolytech.ru/storage/43ec517d68b6edd3015b3edc9a11367b/images/.thumbs/700f9033483987839fa6e638787fba67_0_500_0.jpg"
                    alt="Описание изображения..." style="border: 5px solid #fff;">
                <p class="text-center teacher">Бойкова Галина Васильевна</p>
                <p class="text-center teacher">к.э.н, МПУ</p>
            </div>
            <div class="col-sm-4">
                <img class="shadow-lg img-fluid"
                    src="https://old.mospolytech.ru/storage/43ec517d68b6edd3015b3edc9a11367b/images/.thumbs/700f9033483987839fa6e638787fba67_0_500_0.jpg"
                    alt="Описание изображения..." style="border: 5px solid #fff;">
                <p class="text-center teacher">Бойкова Галина Васильевна</p>
                <p class="text-center teacher">к.э.н, МПУ</p>
            </div>
        </div>

    </div>
