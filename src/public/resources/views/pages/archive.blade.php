@extends('layouts.app1')

@section('title')
	Архив предыдущих конференций
@endsection

@section('content')
<style>
    body{
        background-image: url(http://pictures.std-1056.ist.mospolytech.ru/blackfon.jpg)
    }
	
	.teacher{
		color: white
	}

	h3{
        padding: 10px;
		color:  rgba(135, 75,160)
    }

	.maininfo{
		background-image: url(http://pictures.std-1056.ist.mospolytech.ru/whitefon.jpg)
	}
</style>

<div class="container-fluid mb-3">
    <div class="row">
        <div class = "col-12">
            <h1 class = "text-center" style="margin-top: 100px; background-image: url(https://wallup.net/wp-content/uploads/2017/11/23/433673-minimalism-low_poly.jpg); border-radius: 20px"> Предыдущие конференции </h1>
        </div>
    </div>
</div>

<div class = "container rounded maininfo mb-3">
    <div class = "row">
        <div class = "col-12">
            <h1 style = "font-family: Times New Roman, Georgia, Serif;"> Название конференции </h1>
        </div>
    </div>

    <div class = "row mt-2">
        <div class = "col-md-6 col-12">
        <h3 style = "font-family: Times New Roman, Georgia, Serif;"> Дата проведения </h3>
            <p class = "text-justify"> 
                Информация о прошедшей конференции
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Cum rerum eaque quam quas molestiae molestias, quo eos laboriosam labore minima consequuntur. Molestias minima commodi maiores, ea nam iure? Perspiciatis, veritatis.
                Lorem ipsum, dolor sit amet consectetur adipisicing elit. Aspernatur, debitis aut! Harum corrupti error laborum id atque, quisquam vero at totam neque nobis. Adipisci enim rerum quos iusto! Ea, nostrum!
            </p>
        </div>

        <div class = "col-md-6 col-12">
            <img style = "display: block; margin: auto" src = "https://sun9-29.userapi.com/impg/eDDeka5mwCP9ZY60dJ38nDoMjAfuRJsMuIDf2g/LMj0iDLborI.jpg?size=1280x720&quality=96&sign=086643ea3829065193074c0f035434d2&type=album" width = "100%">
        </div>
    </div>
</div>

<div class = "container rounded maininfo mb-3">
    <div class = "row">
        <div class = "col-12">
            <h1 style = "font-family: Times New Roman, Georgia, Serif;"> Название конференции </h1>
        </div>
    </div>

    <div class = "row mt-2">
        <div class = "col-md-6 col-12">
        <h3 style = "font-family: Times New Roman, Georgia, Serif;"> Дата проведения </h3>
            <p class = "text-justify"> 
                Информация о прошедшей конференции
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Cum rerum eaque quam quas molestiae molestias, quo eos laboriosam labore minima consequuntur. Molestias minima commodi maiores, ea nam iure? Perspiciatis, veritatis.
                Lorem ipsum, dolor sit amet consectetur adipisicing elit. Aspernatur, debitis aut! Harum corrupti error laborum id atque, quisquam vero at totam neque nobis. Adipisci enim rerum quos iusto! Ea, nostrum!
            </p>
        </div>

        <div class = "col-md-6 col-12">
            <img style = "display: block; margin: auto" src = "https://sun9-29.userapi.com/impg/eDDeka5mwCP9ZY60dJ38nDoMjAfuRJsMuIDf2g/LMj0iDLborI.jpg?size=1280x720&quality=96&sign=086643ea3829065193074c0f035434d2&type=album" width = "100%">
        </div>
    </div>
</div>

<div class = "container rounded maininfo mb-3">
    <div class = "row">
        <div class = "col-12">
            <h1 style = "font-family: Times New Roman, Georgia, Serif;"> Название конференции </h1>
        </div>
    </div>

    <div class = "row mt-2">
        <div class = "col-md-6 col-12">
        <h3 style = "font-family: Times New Roman, Georgia, Serif;"> Дата проведения </h3>
            <p class = "text-justify"> 
                Информация о прошедшей конференции
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Cum rerum eaque quam quas molestiae molestias, quo eos laboriosam labore minima consequuntur. Molestias minima commodi maiores, ea nam iure? Perspiciatis, veritatis.
                Lorem ipsum, dolor sit amet consectetur adipisicing elit. Aspernatur, debitis aut! Harum corrupti error laborum id atque, quisquam vero at totam neque nobis. Adipisci enim rerum quos iusto! Ea, nostrum!
            </p>
        </div>

        <div class = "col-md-6 col-12">
            <img style = "display: block; margin: auto" src = "https://sun9-29.userapi.com/impg/eDDeka5mwCP9ZY60dJ38nDoMjAfuRJsMuIDf2g/LMj0iDLborI.jpg?size=1280x720&quality=96&sign=086643ea3829065193074c0f035434d2&type=album" width = "100%">
        </div>
    </div>
</div>