@extends('layouts.AppLK')

@section('title')
	Ответ к комментарию
@endsection

@section('content')

<main class="page-content">
    <div class="container">
        <div class="container">
            @if (Session::has('message'))
                <div class = "alert alert-primary mt-3"> {{ Session::get('message') }} </div>
            @endif
    
            @include('common.errors')

            <form action = "{{ route('SendAnswer') }}" method = "POST">
                {{ csrf_field() }}
                <div class = "form-group">
                    <input type = "hidden" value = "{{ Auth::user()->id }}" name = "user_id">
                    <input type = "hidden" value = "{{ $comment->id }}" name = "comment_id">

                    <div class="col-12">
                        <textarea type="text" name="text" class="form-control" placeholder="Добавить ответ" style = "min-height: 150px"></textarea>
                    </div>
 
                    <div class="col-12 mt-2">
                        <button type="submit" class="form-control btn-primary">
                            <i class="fa fa-plus"></i> Добавить ответ
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</main>

<style>
    body{
        background-image: url(http://pictures.std-1056.ist.mospolytech.ru/blackfon.jpg)
    }

	#cardR{
		background-image: url(http://pictures.std-1056.ist.mospolytech.ru/whitefon.jpg)
	}
</style>

@endsection