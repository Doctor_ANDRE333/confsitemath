@extends('layouts.AppLK')

@section('title')
	Обо мне
@endsection

@section('content')

<main class="page-content">
    <div class="container">
		@include('common.errors')
		
		@if (Session::has('message'))
			<div class = "alert alert-primary mt-3"> {{ Session::get('message') }} </div>
		@endif

		<div class="card text-center" id="cardR">
			<div class="card-header">
				<h5 class="card-title">Изменение пользовательских данных</h5>
			</div>

			<div class="card-body">
			<form action="{{ route('UpdateMe', $user) }}" method="POST" class="form-horizontal mt-3" enctype="multipart/form-data">
			  {{ csrf_field() }}
				<div class="form-group">
					
					<div class="row">
						<div class="col-sm-6" style = "margin: 0 auto">
							<h5><label for="user" class="col control-label mb-0">Имя</label></h5>
						  	<input value="{{ $user->name }}" type="text" name="name" id="user-name" class="form-control mb-3">
						</div>
					</div>
					
					<div class="row">
						<div class="col-sm-6" style = "margin: 0 auto">
							<h5><label for="user" class="col control-label mb-0">Фамилия</label></h5>
						  	<input value="{{ $user->surname }}" type="text" name="surname" id="user-surname" class="form-control mb-3">
						</div>
					</div>
										
					<div class="row">
						<div class="col-sm-6" style = "margin: 0 auto">
							<h5><label for="user" class="col control-label mb-0">Группа</label></h5>
						  	<input value="{{ $user->group }}" type="text" name="group" id="user-group" class="form-control mb-3">
						</div>
					</div>

					<div class="row">
						<div class="col-sm-6" style = "margin: 0 auto">
							<h5><label for="user" class="col control-label mb-0">Email</label></h5>
						  <input value="{{ $user->email }}" type="text" name="email" id="user-email" class="form-control mb-3">
						</div>
					</div>
					
					<div class="row">
						<div class="col-sm-6" style = "margin: 0 auto">
							<h5><label for="user" class="col control-label mb-0">Дата рождения</label></h5>
						  <input value="{{ $user->birth }}" type="date" name="birth" id="user-birth" class="form-control mb-3">
						</div>
					</div>

					<div class="row">
						<div class="col-sm-6" style = "margin: 0 auto">
							<h5><label for="user" class="col control-label mb-0">Сменить пароль?</label><h5>
							<select class = "form-control" id = "passCheck" name = "passCheck">
								<option value="0"> Нет </option>
								<option value="1"> Да </option>
							</select>
						</div>
					</div>

					<div class = "passwords">

					</div>

					<div class="col-5" style = "margin: 0 auto">
					  <button type="submit" class="btn btn-primary mt-4 mb-3 form-control">
						<i class="fa fa-plus"></i> Редактировать 
					  </button>
					</div>
				</div>
			</form>
			</div>

			<div class="card-footer text-muted">
				Московский Политех
			</div>
		</div>
    </div>
</main>

<script>
	let Block = document.querySelector('.passwords');
	console.log(Block);

	function Drop(){
		let password_select = document.getElementById('passCheck').value;
		if (password_select != '1'){
			Block.innerHTML = '';
		}
		else if (password_select == '1' && Block.firstChild == null){
			let newDiv = document.createElement('div');
			newDiv.className = 'passwords';
			newDiv.innerHTML = `
			<div class="row">
				<div class="col-sm-6" style = "margin: 0 auto">
					<h5><label for="user" class="col control-label mb-0">Пароль</label><h5>
					<input value="" type="password" placeholder="Для изменения введите новый или старый пароль" name="password" id="user-password" class="form-control mb-3">
				</div>
			</div>
			
			<div class="row">
				<div class="col-sm-6" style = "margin: 0 auto">
					<h5><label for="user" class="col control-label mb-0">Подтвердите пароль</label><h5>
					<input value="" type="password" placeholder="Подтвердите пароль" name="password_confirmation" id="user-password-confirmation" class="form-control mb-3">
				</div>
			</div>
			`
			console.log(newDiv);
			Block.append(newDiv);
			}
		}
	
	setInterval(Drop, 250);
</script>

<style>
    body{
        background-image: url(http://pictures.std-1056.ist.mospolytech.ru/blackfon.jpg)
    }

	#cardR{
		background-image: url(http://pictures.std-1056.ist.mospolytech.ru/whitefon.jpg)
	}

	.card-header h5{
		color:  rgba(135, 75,160)
	}
</style>
@endsection