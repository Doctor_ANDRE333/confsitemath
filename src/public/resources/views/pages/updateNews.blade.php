@extends('layouts.app1')

@section('title')
	Обновление новости
@endsection

@section('content')
	<div class = "container" style = "min-height: 700px; margin-top: 100px">
        <div class="card text-white bg-dark mt-5" style = "width: 75%; margin: 0 auto">
            <div class="card-header">
                <h5 class = "text-center"> Обновление новости </h5>
            </div>

            <div class="card-body">
                <form action="{{ route('news.update', $currNews->id) }}" method="POST" class="form-horizontal mt-3">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}

                    <div class = "row">
                        <div class = "col-12">
                            <b> Название: </b> 
        
                            <input type = "text" value="{{ $currNews->label }}" name = "label" class = "form-control">
                        </div>

                        <div class = "col-12">
                            <b> Изображение: </b>
        
                            <input type = "text" value="{{ $currNews->img }}" name = "img" class = "form-control">
                        </div>
                    </div>

                    <div class = "row mt-2">
                        <div class = "col">
                            <b> Текст: </b> 
                            <textarea type = "text" name = "text" class = "form-control" style = "max-height: 100px; overflow-y: auto" placeholder="Ваш текст..."></textarea>
                            <input type = "submit" name = "Sub" value = "Обновить" class = "form-control mt-3 btn btn-primary" >
                        </div>
                    </div>
                </form>
            </div>
        </div>
	</div>
	
	<style>
		body{
			background-image: url(https://avatanplus.com/files/resources/original/5b88ffcc57cde1658f273648.jpg);
		}
	</style>

@endsection