@extends('layouts.app1')

@section('title')
    О конференции
@endsection

@section('content')

    <div class="container-fluid mb-3">
        <div class="row">
            <div class="col-12">
                <h1 class="text-center"
                    style="margin-top: 100px; background-image: url(https://wallup.net/wp-content/uploads/2017/11/23/433673-minimalism-low_poly.jpg); border-radius: 20px">
                    О конференции </h1>
            </div>
        </div>
    </div>

    <div class="container rounded myCard">
        <div class="row">
            <div class="col-12">
                <h1 style="font-family:'Gilroy-Bold'"> Данные </h1>
            </div>
        </div>

        <div class="row mt-2">
            <div class="col-md-6 col-12">
                <p class="text-justify">
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Cum rerum eaque quam quas molestiae molestias,
                    quo eos laboriosam labore minima consequuntur. Molestias minima commodi maiores, ea nam iure?
                    Perspiciatis, veritatis.
                    Lorem ipsum, dolor sit amet consectetur adipisicing elit. Aspernatur, debitis aut! Harum corrupti error
                    laborum id atque, quisquam vero at totam neque nobis. Adipisci enim rerum quos iusto! Ea, nostrum!
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Consequuntur neque voluptate minus aut,
                    accusamus animi veniam provident quas omnis repudiandae laudantium amet facilis illum numquam quo
                    eligendi tempora quod modi.
                    Lorem ipsum dolor, sit amet consectetur adipisicing elit. Tempore minus atque iste rem, quod doloremque
                    earum ad rerum quis adipisci qui consequuntur, id molestiae, dolorem hic asperiores possimus officiis
                    ea.
                    Lorem ipsum, dolor sit amet consectetur adipisicing elit. Corporis, accusamus. Dolorum corporis minus
                    qui atque hic quo accusamus ad, eaque quasi, illo est eum at provident. Magni molestias odio iusto.
                </p>
            </div>

            <div class="col-md-6 col-12">
                <img class="mt-2 p-1 rounded mb-1" src="http://pictures.std-1056.ist.mospolytech.ru/conf.jpg" width="100%"
                    style="border: 2px solid rgba(135, 75, 160);">
            </div>
        </div>
    </div>

    <div class="container rounded myCard mb-3 mt-3">
        <div class="row">
            <div class="col-12">
                <h1 style="font-family: Gilroy-Light;"> Данные два </h1>
            </div>
        </div>

        <div class="row mt-2">
            <div class="col-md-6 col-12">
                <p class="text-justify">
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Cum rerum eaque quam quas molestiae molestias,
                    quo eos laboriosam labore minima consequuntur. Molestias minima commodi maiores, ea nam iure?
                    Perspiciatis, veritatis.
                    Lorem ipsum, dolor sit amet consectetur adipisicing elit. Aspernatur, debitis aut! Harum corrupti error
                    laborum id atque, quisquam vero at totam neque nobis. Adipisci enim rerum quos iusto! Ea, nostrum!
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Consequuntur neque voluptate minus aut,
                    accusamus animi veniam provident quas omnis repudiandae laudantium amet facilis illum numquam quo
                    eligendi tempora quod modi.
                    Lorem ipsum dolor, sit amet consectetur adipisicing elit. Tempore minus atque iste rem, quod doloremque
                    earum ad rerum quis adipisci qui consequuntur, id molestiae, dolorem hic asperiores possimus officiis
                    ea.
                    Lorem ipsum, dolor sit amet consectetur adipisicing elit. Corporis, accusamus. Dolorum corporis minus
                    qui atque hic quo accusamus ad, eaque quasi, illo est eum at provident. Magni molestias odio iusto.
                </p>
            </div>

            <div class="col-md-6 col-12">
                <img class="mt-2 p-1 rounded mb-1" src="http://pictures.std-1056.ist.mospolytech.ru/conf.jpg" width="100%"
                    style="border: 2px solid rgba(135, 75, 160);">
            </div>
        </div>
    </div>

    <style>
        body {
            background-image: url(http://pictures.std-1056.ist.mospolytech.ru/blackfon.jpg)
        }

        .myCard {
            background-image: url(http://pictures.std-1056.ist.mospolytech.ru/whitefon.jpg)
        }

    </style>

@endsection
