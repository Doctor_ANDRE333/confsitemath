@extends('layouts.app1')

@section('content')

<div class="container" style = "min-height: 700px">
    <div class="row" style = "margin-top: 225px">
        <div class="col-md-6" style = "margin: 0 auto">
            <form class="form-horizontal" style = "padding-bottom: 1px" role="form" method="POST" action="{{ url('/register') }}">
                {{ csrf_field() }}
                <span class="heading">Регистрация</span>

                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus placeholder="Имя">
                    <i class="fas fa-user"></i>

                    @if ($errors->has('name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus placeholder="Email">
                    <i class="fas fa-envelope"></i>

                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <input id="password" type="password" class="form-control" name="password" required autofocus placeholder="Пароль">
                    <i class="fas fa-key"></i>

                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group">
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autofocus placeholder="Подтверждение пароля">
                    <i class="fas fa-key"></i>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn">
                        Регистрация
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>

<style>
    /* Demo Background */
    body{
        background-image: url(http://pictures.std-1056.ist.mospolytech.ru/blackfon.jpg)
    }
    
    /* Form Style */
    .form-horizontal{
        background: #fff;
        padding-bottom: 40px;
        border-radius: 15px;
        text-align: center;
    }

    .form-horizontal .heading{
        display: block;
        font-size: 25px;
        font-weight: 700;
        padding: 35px 0;
        border-bottom: 1px solid #f0f0f0;
        margin-bottom: 30px;
    }

    .form-horizontal .form-group{
        padding: 0 40px;
        margin: 0 0 25px 0;
        position: relative;
    }

    .form-horizontal .form-control{
        background: #f0f0f0;
        border: none;
        border-radius: 20px;
        box-shadow: none;
        padding: 0 20px 0 45px;
        height: 40px;
        transition: all 0.3s ease 0s;
    }

    .form-horizontal .form-control:focus{
        background: #e0e0e0;
        box-shadow: none;
        outline: 0 none;
    }

    .form-horizontal .form-group i{
        position: absolute;
        top: 12px;
        left: 60px;
        font-size: 17px;
        color: #c8c8c8;
        transition : all 0.5s ease 0s;
    }

    .form-horizontal .form-control:focus + i{
        color: rgba(135, 75,160);
    }

    .form-horizontal .fa-question-circle{
        display: inline-block;
        position: absolute;
        top: 12px;
        right: 60px;
        font-size: 20px;
        color: #808080;
        transition: all 0.5s ease 0s;
    }

    .form-horizontal .fa-question-circle:hover{
        color: #000;
    }

    .form-horizontal .main-checkbox{
        float: left;
        width: 20px;
        height: 20px;
        background: rgba(135, 75,160);
        border-radius: 50%;
        position: relative;
        margin: 5px 0 0 5px;
        border: 1px solid rgba(135, 75,160);
    }

    .form-horizontal .main-checkbox label{
        width: 20px;
        height: 20px;
        position: absolute;
        top: 0;
        left: 0;
        cursor: pointer;
    }

    .form-horizontal .main-checkbox label:after{
        content: "";
        width: 10px;
        height: 5px;
        position: absolute;
        top: 5px;
        left: 4px;
        border: 3px solid #fff;
        border-top: none;
        border-right: none;
        background: transparent;
        opacity: 0;
        -webkit-transform: rotate(-45deg);
        transform: rotate(-45deg);
    }

    .form-horizontal .main-checkbox input[type=checkbox]{
        visibility: hidden;
    }

    .form-horizontal .main-checkbox input[type=checkbox]:checked + label:after{
        opacity: 1;
    }

    .form-horizontal .text{
        float: left;
        margin-left: 7px;
        line-height: 20px;
        padding-top: 5px;
        text-transform: capitalize;
    }

    .form-horizontal .btn{
        width: 110px;
        height: 35px;
        float: right;
        font-size: 14px;
        color: #fff;
        background: rgba(135, 75,160);
        border-radius: 30px;
        display: flex;
        align-items: center;
        justify-content: center;
    }

    @media only screen and (max-width: 479px){
        .form-horizontal .form-group{
            padding: 0 25px;
        }

        .form-horizontal .form-group i{
            left: 45px;
        }

        .form-horizontal .btn{
            padding: 10px 20px;
        }
    }
</style>

@endsection
