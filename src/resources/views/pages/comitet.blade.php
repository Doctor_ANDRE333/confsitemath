@extends('layouts.app1')

@section('title')
    Информация о комитете
@endsection

@section('content')

    <style>
        body {
            background-image: url(http://pictures.std-1056.ist.mospolytech.ru/blackfon.jpg)
        }

        .teacher {
            color: white
        }

    </style>

    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <h1 class="text-center"
                    style="margin-top: 100px; background-image: url(https://wallup.net/wp-content/uploads/2017/11/23/433673-minimalism-low_poly.jpg); border-radius: 20px">
                    Организационный комитет </h1>
            </div>
        </div>
    </div>
    <br>
    <br>
    <div class="container mb-5" style="max-width: 1000px;">

        <div class="row mb-4">
            <div class="col-sm-4">
                <img class="shadow img-fluid"
                    src="http://pictures.std-1056.ist.mospolytech.ru/prepods/Muhanov.jpg"
                    alt="Описание изображения..." style="border: 5px solid #fff;">
                <p class="text-center teacher">Муханов Сергей Александрович</p>
                <p class="text-center teacher">к.п.н., доцент, доцент кафедры «Математика»,
                    руководитель секции №2;</p>
            </div>
            <div class="col-sm-4">
                <img class="shadow-sm img-fluid"
                    src="http://pictures.std-1056.ist.mospolytech.ru/prepods/Andreev.jpg"
                    alt="Описание изображения..." style="border: 5px solid #fff;">
                <p class="text-center teacher">Андреев Степан Николаевич</p>
                <p class="text-center teacher">д.ф.-м.н., зав. кафедрой «Математика» (председатель)</p>
            </div>
            <div class="col-sm-4">
                <img class="shadow-lg img-fluid"
                    src="http://pictures.std-1056.ist.mospolytech.ru/prepods/Bojkova.jpg"
                    alt="Описание изображения..." style="border: 5px solid #fff;">
                <p class="text-center teacher">Бойкова Галина Васильевна</p>
                <p class="text-center teacher">к.э.н., доцент, доцент кафедры «Математика»,
                    ответственный редактор Сборника трудов конференции</p>
            </div>
        </div>

        <div class="row mb-4">
            <div class="col-sm-4">
                <img class="shadow-sm img-fluid"
                    src="http://pictures.std-1056.ist.mospolytech.ru/prepods/Kogan.jpg"
                    alt="Описание изображения..." style="border: 5px solid #fff;">
                <p class="text-center teacher">Коган Ефим Александрович</p>
                <p class="text-center teacher">к.ф.-м.н., доцент, доцент кафедры «Математика»,
                    ответственный секретарь конференции</p>
            </div>
            <div class="col-sm-4">
                <img class="shadow-lg img-fluid"
                    src="http://pictures.std-1056.ist.mospolytech.ru/prepods/Samohin.jpg"
                    alt="Описание изображения..." style="border: 5px solid #fff;">
                <p class="text-center teacher">Самохин Вячеслав Николаевич</p>
                <p class="text-center teacher">д.ф.-м.н., профессор, профессор кафедры «Математика»,
                    руководитель секции №1</p>
            </div>
            <div class="col-sm-4">
                <img class="shadow img-fluid"
                    src="http://pictures.std-1056.ist.mospolytech.ru/prepods/Enikeev.jpg"
                    alt="Описание изображения..." style="border: 5px solid #fff;">
                <p class="text-center teacher">Еникеев Ильдар Хасанович</p>
                <p class="text-center teacher">д.т.н., профессор, профессор кафедры «Математика»,
                    руководитель секции №3</p>
            </div>
        </div>

    </div>
