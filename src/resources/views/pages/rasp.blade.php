@extends('layouts.app1')

@section('title')
	Расписание
@endsection

@section('content')

<div class="container-fluid mb-3">
    <div class="row">
        <div class = "col-12">
            <h1 class = "text-center" style="margin-top: 100px; background-image: url(https://wallup.net/wp-content/uploads/2017/11/23/433673-minimalism-low_poly.jpg); border-radius: 20px"> Расписание </h1>
        </div>
    </div>
</div>

<div class = "container maininfo mb-3">
    <div class = "table-responsive pt-3 pb-2">
        <table class="table table-striped">
            <thead class="thead-light" style = "border-bottom: 3px solid rgba(135, 75,160);">
                <tr class = "text-center">
                    <th scope="col">Время (начало)</th>
                    <th scope="col" style = "vertical-align: middle;">Действие</th>
                    <th scope="col" style = "vertical-align: middle;">Руководитель</th>
                </tr>
            </thead>

            <tbody>
                <tr class = "text-center">
                    <td> <b> 10.00  </b> </td>
                    <td> <b> Открытие конференции </b> </td>
                    <td> <b> Андреев С.Н. </b ></td>
                </tr>

                <tr class = "text-center">
                    <td> <b> 10.15 – 13.00  </b> </td>
                    <td> <b> Заседание секции 1 «Математическое моделирование прикладных задач» </b> </td>
                    <td rowspan = "3" style = "vertical-align: middle;"> <b> Самохин В.Н.  </b ></td>
                </tr>

                <tr class = "text-center">
                    <td> <b> 13.00 – 14.00  </b> </td>
                    <td> <b> Перерыв </b> </td>
                </tr>

                <tr class = "text-center">
                    <td> <b> 14.00 – 17.30  </b> </td>
                    <td> <b> Заседание секции 1 (продолжение) </b> </td>
                </tr>

                <tr class = "text-center">
                    <td> <b> 10.15 – 13.00  </b> </td>
                    <td> <b> Заседание секции 2 «Проблемы и перспективы развития математического образования в высшей школе» </b> </td>
                    <td rowspan = "3" style = "vertical-align: middle;"> <b> Муханов С.А. </b ></td>
                </tr>

                <tr class = "text-center">
                    <td> <b> 13.00 – 14.00 </b> </td>
                    <td> <b> Перерыв </b> </td>
                </tr>

                <tr class = "text-center">
                    <td> <b> 14.00 – 17.30  </b> </td>
                    <td> <b> Заседание секции 2 (продолжение) </b> </td>
                </tr>

                <tr class = "text-center">
                    <td> <b> 10.15 – 13.00  </b> </td>
                    <td> <b> Cекция 3 (студенческая) (по тематике конференции в целом) </b> </td>
                    <td rowspan = "3" style = "vertical-align: middle;"> <b> Еникеев И.Х. </b ></td>
                </tr>

                <tr class = "text-center">
                    <td> <b> 13.00 – 14.00 </b> </td>
                    <td> <b> Перерыв </b> </td>
                </tr>

                <tr class = "text-center">
                    <td> <b> 14.00 – 17.30  </b> </td>
                    <td> <b> Заседание секции 3 (продолжение) </b> </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

<style>
    body{
        background-image: url(http://pictures.std-1056.ist.mospolytech.ru/blackfon.jpg);
    }
	
	.teacher{
		color: white;
	}

	h3{
        padding: 10px;
		color:  rgba(135, 75,160);
    }

	.maininfo{
		background-image: url(http://pictures.std-1056.ist.mospolytech.ru/whitefon.jpg);
	}

    table{
        border: 3px solid rgba(135, 75,160);
    }

    td{
        border: 3px solid rgba(135, 75,160);
    }

    
</style>
@endsection