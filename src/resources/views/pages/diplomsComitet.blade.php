@extends('layouts.AppLK')

@section('title')
	Дипломы и сертификаты
@endsection

@section('content')

<main class="page-content">
    <div class="container fon">
		@include('common.errors')
		
		@if (Session::has('message'))
			<div class = "alert alert-primary mt-3"> {{ Session::get('message') }} </div>
		@endif
		
		<form action = "{{ route('makeDiplom') }}" method = "POST">
		 	{{ csrf_field() }}

			<div class="row">
				<div class="col-8">
					<!--<h3 class = "text-center">Участник </h3>-->
					<select name="user" class="form-control">
						@foreach ($users as $user)
						<option  value="{{ $user->id }}">{{ $user->name . " " . $user->surname . " " . $user->group }}</option>
						@endforeach
					</select>
				</div>

				<div class="col-4">
					<button type="submit" class="form-control btn create"> Создать</button>
				</div>
			</div>
		</form>

		<hr>

		<div class="row text-center">
			<div class="col-12">
				<div class = "card mt-3 text-white bg-dark">
					<div class = "card-header dark">
						<h3>Созданные сертификаты и дипломы</h3>
					</div>
				</div>

				<div class = "table-responsive">
					<table class="table table-striped">
						<thead class="thead-light">
							<tr class = "text-center">
								<th scope="col">Данные</th>
								<th scope="col">Дата создания</th>
								<th scope="col">Скачать файл</th>
							</tr>
						</thead>

						<tbody>
							@foreach ($diploms as $diplom) 
								<tr class = "text-center">
									<td> <b> {{$diplom->User->name . " " . $diplom->User->surname . " " . $diplom->User->group }} </b> </td>
									<td> <b> {{$diplom->created_at}} </b> </td>
									<td><button class="btn btn"type="submit" method="GET" onclick="location.href='{{ route('getDiplom', $diplom->id) }}'">{{$diplom->filename}}</button></td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
    </div>
</main>

<style>
    body{
        background-image: url(http://pictures.std-1056.ist.mospolytech.ru/blackfon.jpg)
    }
	
	.fon{
		background-image: url(http://pictures.std-1056.ist.mospolytech.ru/whitefon.jpg);
		border-radius: 5px;
	}

	td>.btn{
		background-color:  rgba(135, 75,160);
		color: white;
		min-width: 300px;
	}

	tr.text-center {
		background-color: rgb(255, 255, 255);
	}

	tr:nth-child(2n) {
		background-color: rgb(220, 220, 220);
	}

	.create{
		background-color:  rgba(135, 75,160);
		color: white;
	}
</style>

@endsection