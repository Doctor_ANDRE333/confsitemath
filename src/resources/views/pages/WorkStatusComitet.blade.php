@extends('layouts.AppLK')

@section('title')
	Статус работы
@endsection

@section('content')

<main class="page-content">
    <div class="container">
		@include('common.errors')
		
		@if (Session::has('message'))
			<div class = "alert alert-primary mt-3"> {{ Session::get('message') }} </div>
		@endif

        <div class="card text-center" id="cardR">
			<div class="card-header">
				<h5 class="card-title">Просмотр статуса работы</h5>
			</div>

			<div class="card-body">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                    <tr>
                        <td>&nbsp;</td>
                        <td width="1050"><p align="center" style="border-bottom:2px solid"><img src="http://pictures.std-1056.ist.mospolytech.ru/polytech_logo_dark.png" height="111" vspace="10"></p></td>
                        <td>&nbsp;</td>
                    </tr>

                    <tr>
                        <td>&nbsp;</td>
                        <td>
                        <h3>Статус загруженной работы</h3>
                        <p>Данный сервис позволяет посмотреть статус загруженных работ.</p>
						@if (count($reports) == 0) 
							<div>
								<b>Ещё не загружено ни одной работы :(</b>
							</div>
						@else
                        <table border="0" cellspacing="0" cellpadding="5" align="left">
                            <tr style="font-weight:bold; color: rgba(135, 75,160)">
                                <td style="border-bottom:1px solid">Автор</td>
                                <td style="border-bottom:1px solid">Учебная группа</td>
                                <td style="border-bottom:1px solid">Секция</td>
                                <td style="border-bottom:1px solid">Работа</td>
                                <!-- <td style="border-bottom:1px solid">Спецноминации</td> -->
                                <td style="border-bottom:1px solid">Соавторы</td>
                                <td align="center" style="border-bottom:1px solid">Статус</td>
								
                                <tr>
                                    @foreach($reports as $report)
                                        <td width="350" style="border-bottom:1px dashed" >{{ $report->user->surname }}&nbsp;{{ $report->user->name }}</td>
                                        <td width="100" style="border-bottom:1px dashed">{{ $report->group }}</td>
                                        <td width="350" style="border-bottom:1px dashed">{{ $report->topic }}</td>
                                        <td width="350" style="border-bottom:1px dashed">{{ $report->label }}</td>
                                        <td width="350" style="border-bottom:1px dashed">{{ $report->collaborator or '' }}</td>
                                        <td align="center" width="350" style="border-bottom:1px dashed">
                                            <button type="submit" method="GET" class="mt-3" onclick="location.href='{{ route('getFile', $report->id) }}'">Скачать работу </button>

                                            <form action="{{route('editStatus', $report->id)}}" method="POST" class="form-horizontal mt-3">
                                                {{ csrf_field() }}
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <select name="status" class="form-control">
                                                                <option selected value="{{ $report->status }}">{{ $report->status }}</option>
                                                                @if ($report->status == 'На рассмотрении')
                                                                    <option value="Работа одобрена">Работа одобрена</option>
                                                                    <option value="Работа отклонена">Работа отклонена</option>
                                                                @elseif ($report->status == 'Работа одобрена') 
                                                                    <option value="На рассмотрении">На рассмотрении</option>
                                                                    <option value="Работа отклонена">Работа отклонена</option>
                                                                @elseif ($report->status == 'Работа отклонена')
                                                                    <option value="На рассмотрении">На рассмотрении</option>
                                                                    <option value="Работа одобрена">Работа одобрена</option>
                                                                @endif
                                                            </select>
                                                        </div>
                                                        <div class="col-sm-12">
                                                            <button type="submit" class="mt-3">
                                                            Применить 
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </td>
                                  
                                </tr>
								  @endforeach 
							</tr>
						</table><br><br><br><br>
						@endif
                    </tr>
                </table>
            </div>

            <div class="card-footer text-muted">
				Московский Политех
			</div>
        </div>
    </div>
</main>

<style>
    body{
        background-image: url(http://pictures.std-1056.ist.mospolytech.ru/blackfon.jpg)
    }

	#cardR{
		background-image: url(http://pictures.std-1056.ist.mospolytech.ru/whitefon.jpg)
	}

	.card-header h5{
		color:  rgba(135, 75,160)
	}

    .bkgr_lt {
        background-image: url(img/bkgr_lt.png);
        background-repeat: repeat-y;
        background-position: right;
    }

    .bkgr_rt {
        background-image: url(img/bkgr_rt.png);
        background-repeat: repeat-y;
        background-position: left;
    }

    .text {
        font-family: "Arial Unicode MS";
        font-size: 16px;
    }

    .copyright {
        font-family: "Arial Unicode MS";
        font-size: 10px;
        text-align: center;
    }

    .menu_sntk, .menu_sntk a {
        text-decoration: none;
        font-weight: bold;
        font-family: "Arial Unicode MS";
        font-size: 14px;
    }

    .menu_sntk a:hover {
        border-bottom:1px thin;
    }

    .menu_sntka, .menu_sntka a {
        text-decoration: none;
        font-weight: normal;
        font-family: "Arial Unicode MS";
        font-size: 14px;
    }

    .menu_sntka a:hover {
        text-decoration: underline;
    }

    #users tr {
        background-color: #DFDFDF;
    }

    .recommended td {
        background-color: #9ad176;
    }

    .winner td {
        background-color: #e6c00f;
    }
</style>
@endsection