@extends('layouts.AppLK')

@section('title')
	Статус работы
@endsection

@section('content')

<main class="page-content">
    <div class="container">
		@include('common.errors')
		
		@if (Session::has('message'))
			<div class = "alert alert-primary mt-3"> {{ Session::get('message') }} </div>
		@endif
		
		@if (Session::has('error'))
			<div class = "alert alert-danger mt-3"> {{ Session::get('error') }} </div>
		@endif
		@foreach ($reports as $report)
		@if ($report->status == 'Работа отклонена') 
			<div class = "alert alert-danger mt-3"> Ваша работа отклонена! Обратитесь в чат за подробной информацией! </div>
		@endif
		@endforeach

        <div class="card text-center" id="cardR">
			<div class="card-header">
				<h5 class="card-title">Просмотр статуса работы</h5>
			</div>

			<div class="card-body">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                    <tr>
                        <td>&nbsp;</td>
                        <td width="1050"><p align="center" style="border-bottom:2px solid"><img src="http://pictures.std-1056.ist.mospolytech.ru/polytech_logo_dark.png" height="111" vspace="10"></p></td>
                        <td>&nbsp;</td>
                    </tr>

                    <tr>
                        <td>&nbsp;</td>
                        <td>
                        <h3>Статус загруженной работы</h3>
                        <p>Данный сервис позволяет посмотреть статус загруженных работ.</p>
						@if ( count(Auth::user()->Report) > 0 )
							<div class = "btn btn create form-control">Вы уже загрузили одну работу</div>
						@else
							<a type="submit" href = "{{ route('works.create') }}" class="mb-2 btn create form-control">Добавить работу </a>
						@endif
						
						@if (count($reports) == 0)
							
						@else
                        <table border="0" cellspacing="0" cellpadding="5" align="left">
                            <tr style="font-weight:bold; color: rgba(135, 75,160)">
                                <td style="border-bottom:1px solid">Автор</td>
                                <td style="border-bottom:1px solid">Учебная группа</td>
                                <td style="border-bottom:1px solid">Секция</td>
                                <td style="border-bottom:1px solid">Работа</td>
                                <!-- <td style="border-bottom:1px solid">Спецноминации</td> -->
                                <td style="border-bottom:1px solid">Соавторы</td>
                                <td align="center" style="border-bottom:1px solid">Статус</td>
								
                                <tr valign="top" style="font-family:'Arial Narrow'">
                                    @foreach($reports as $report)
                                        <td  width="350" style="border-bottom:1px dashed" >{{ $report->user->surname }}&nbsp;{{ $report->user->name }}</td>
                                        <td width="100" style="border-bottom:1px dashed">{{ $report->group }}</td>
                                        <td width="350" style="border-bottom:1px dashed">{{ $report->section_number }}</td>
                                        <td width="350" style="border-bottom:1px dashed">{{ $report->label }}</td>
                                        <td width="350" style="border-bottom:1px dashed">{{ $report->collaborator or '' }}</td>
										@if ($report->status == 'Работа одобрена')
											<td class="text-success" align="center" width="350" style="border-bottom:1px dashed"> <h6>{{ $report->status }}</h6>
											
										@elseif ($report->status == 'Работа отклонена')
											<td class="text-danger" align="center" width="350" style="border-bottom:1px dashed"><h6>{{ $report->status }}</h6>
										@else
											<td align="center" width="350" style="border-bottom:1px dashed"><h6>{{ $report->status }}</h6> 
										@endif
												<button type="submit" class="mb-2" onclick="location.href='{{route('works.edit', $report->id)}}'">Редактировать работу </button>
											</td>
                                </tr>
							@endforeach
							</tr>
						</table><br><br><br><br>
						@endif
                    </tr>
                </table>
            </div>

            <div class="card-footer text-muted">
				Московский Политех
			</div>
        </div>
    </div>
</main>

<style>
    body{
        background-image: url(http://pictures.std-1056.ist.mospolytech.ru/blackfon.jpg)
    }

    .btn{
		background-color:  rgba(135, 75,160);
		color: white;
	}

	#cardR{
		background-image: url(http://pictures.std-1056.ist.mospolytech.ru/whitefon.jpg)
	}

	.card-header h5{
		color:  rgba(135, 75,160)
	}

    .bkgr_lt {
        background-image: url(img/bkgr_lt.png);
        background-repeat: repeat-y;
        background-position: right;
    }

    .bkgr_rt {
        background-image: url(img/bkgr_rt.png);
        background-repeat: repeat-y;
        background-position: left;
    }

    .text {
        font-family: "Arial Unicode MS";
        font-size: 16px;
    }

    .copyright {
        font-family: "Arial Unicode MS";
        font-size: 10px;
        text-align: center;
    }

    .menu_sntk, .menu_sntk a {
        text-decoration: none;
        font-weight: bold;
        font-family: "Arial Unicode MS";
        font-size: 14px;
    }

    .menu_sntk a:hover {
        border-bottom:1px thin;
    }

    .menu_sntka, .menu_sntka a {
        text-decoration: none;
        font-weight: normal;
        font-family: "Arial Unicode MS";
        font-size: 14px;
    }

    .menu_sntka a:hover {
        text-decoration: underline;
    }

    #users tr {
        background-color: #DFDFDF;
    }

    .recommended td {
        background-color: #9ad176;
    }

    .winner td {
        background-color: #e6c00f;
    }
</style>
@endsection