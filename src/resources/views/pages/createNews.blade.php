@extends('layouts.app1')

@section('title')
	Создание новости
@endsection

@section('content')
	<div class = "container" style = "min-height: 700px; margin-top: 100px">
        <div class="card text-dark bg-light mt-5" style = "width: 75%; margin: 0 auto">
            @include('common.errors')
            
            <div class="card-header">
                <h3 class = "text-center"> Создание новости </h3>
            </div>

            <div class="card-body">

                <form action="{{ route('news.store') }}" method="POST" class="form-horizontal mt-3">
                    {{ csrf_field() }}

                    <div class = "row">
                        <div class = "col-12">
                            <b> Название: </b> 
        
                            <input type = "text" value="{{ old('label') }}" name = "label" class = "form-control">
                        </div>

                        <div class = "col-12 mt-2">
                            <b> Изображение: </b>
        
                            <input type = "text" value="{{ old('img') }}" name = "img" class = "form-control">
                        </div>
                    </div>

                    <div class = "row mt-2">
                        <div class = "col">
                            <b> Текст: </b> 
                            <textarea type = "text" name = "text" class = "form-control" style = "max-height: 100px; overflow-y: auto" placeholder="Ваш текст..."></textarea>
        
                            <button type = "submit" name = "Sub" class = "form-control mt-3 btn create">
                                <i class="fa fa-plus"></i> Создать 
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
	</div>
	
	<style>
		body{
            background-image: url(http://pictures.std-1056.ist.mospolytech.ru/blackfon.jpg)
        }

        h3{
            color: rgba(135, 75,160);
        }

        .create{
            background-color: rgba(135, 75,160);
            color: white;
        }
	</style>

@endsection