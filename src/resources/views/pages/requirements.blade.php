@extends('layouts.app1')

@section('title')
	Требования к статьям
@endsection

@section('content')



<div class="container-fluid mb-3">
    <div class="row">
        <div class = "col-12">
            <h1 class = "text-center" style="margin-top: 100px; background-image: url(https://wallup.net/wp-content/uploads/2017/11/23/433673-minimalism-low_poly.jpg); border-radius: 20px"> Требования к статьям на конференцию </h1>
        </div>
    </div>
</div>

<div class = "container rounded maininfo mb-12">
    <div class = "row">
        <div class = "col-12">
            <h1 class="text-center"> Основные требования </h1>
        </div>
    </div>
	<hr>

    <div class = "row mt-12">
        <div class = "col-md-12 col-12">
        <h4 class = "title"> <strong> 1. Требования по оригинальности </strong> </h4>
            <h6> <p class = "text-justify"> 
               Оригинальность текста работы должна быть не менее 70 %.
            </p>
			</h6>
        </div>

        <div class = "col-md-12 col-12">
		<hr>
        <h4 class = "title"> <strong> 2. Общие требования по оформлению </strong> </h4>

        <h6> <p class = "text-justify"> 
            <strong>Материалы статьи должны полностью удовлетворять всем приводимым 
                ниже требованиям. Страницы не нумеровать. </strong>
        </p> 
        </h6>

           <h6> <p class = "text-justify"> 
               Объем материалов – от 5 стр. до 6 стр. Набираются в формате А4; <br>
			поля: левое – 2,5 см, правое, верхнее и нижнее – 2,0 см; <br>
		шрифт Times New Roman, кегль 14, через 1,5 интервала в редакторе MSWord
2010 и выше; <br>
         аннотация,  ключевые слова – кегль 12, через одинарный интервал;
        список литературы  – по центру, кегль 12, через одинарный интервал, без 
        свободных строк до и после этого заголовка.
Абзацный отступ – 1,0 см. <br><br>
 Таблицы, рисунки, графики, диаграммы должны быть пронумерованы, 
подписаны и набраны в черно-белом цвете. <br><br>

Сначала печатаются:<br>
УДК………..		<br>
Фамилия И.О.  <br>
Полное наименование организации без сокращений,  <br>
электронный адрес участника <br><br>

Далее через пробел (1 строка, кегль 14) по центру полужирным шрифтом прописными буквами печатается название статьи (без точки в конце).
Далее Аннотация (5-10 строк) и Ключевые слова (5-10). Печатается – кегль 12, интервал – одинарный.
Потом через пробел (1 строка, кегль 14) печатается основной текст статьи, выравненный по ширине.
Ссылки на литературу даются в тексте в квадратных скобках. 
Список литературы приводится по порядку цитирования в конце статьи в соответствии с ГОСТ Р 7.0.100–2018. Кегль 12, интервал – одинарный
	</h6>
            </p>
        </div>

        <div class = "col-md-12 col-12">
		<hr>
        <h4 class = "title"> <strong> Обязательное требование </strong> </h4>
            <p class = "text-justify"> 
                <b>После завершения текста всей статьи и литературы на русском языке вставляется перевод на английский язык:
				«шапки» статьи,   анннотации,   ключевых слов</b>
            </p>
        </div>
	</div>
		<div class="row ml-2 mr-2">
        <div class = "col-md-6 col-sm-12">
            <h5 class = "text-center">
                <a href="{{ route('getOsnova') }}" class="btn but">Скачать основные положения в формате .docx</a>
            </h5>
        </div>
		<div class = "col-md-6 col-sm-12">
            <h5 class = "text-center">
                <a href="{{ route('getPrimer') }}" class="btn but">Скачать пример статьи в формате .docx</a>
            </h5>
        </div>
		</div>
</div>

<br>
<br>

<style>
    body{
        background-image: url(http://pictures.std-1056.ist.mospolytech.ru/blackfon.jpg)
    }
	
	.teacher{
		color: white
	}

	.title{
        padding: 10px;
		color:  rgba(135, 75,160)
    }

	.maininfo{
		background-image: url(http://pictures.std-1056.ist.mospolytech.ru/whitefon.jpg)
	}
	
	.but{
			background-color: rgba(135,75,160);
			color: white;
	}
</style>
@endsection