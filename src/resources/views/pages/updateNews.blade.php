@extends('layouts.app1')

@section('title')
	Обновление новости
@endsection

@section('content')
	<div class = "container" style = "min-height: 700px; margin-top: 100px">
        <div class="card text-dark bg-light mt-5" style = "width: 75%; margin: 0 auto">
            @include('common.errors')

            <div class="card-header">
                <h3 class = "text-center"> Обновление новости </h3>
            </div>

            <div class="card-body">
                <form action="{{ route('news.update', $currNews->id) }}" method="POST" class="form-horizontal mt-3">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}

                    <div class = "row">
                        <div class = "col-12">
                            <b> Название: </b> 
        
                            <input type = "text" value="{{ $currNews->label }}" name = "label" class = "form-control">
                        </div>

                        <div class = "col-12 mt-2">
                            <b> Изображение: </b>
        
                            <input type = "text" value="{{ $currNews->img }}" name = "img" class = "form-control">
                        </div>
                    </div>

                    <div class = "row mt-2">
                        <div class = "col">
                            <b> Текст: </b> 
                            <textarea type = "text" name = "text" class = "form-control" style = "max-height: 100px; overflow-y: auto" value="{{ $currNews->text }}">{{ $currNews->text }}</textarea>

                            <button type = "submit" name = "Sub" class = "form-control mt-3 btn update">
                                <i class="fas fa-pencil-alt"></i> Обновить
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
	</div>
	
	<style>
		body{
            background-image: url(http://pictures.std-1056.ist.mospolytech.ru/blackfon.jpg)
        }

        h3{
            color: rgba(135, 75,160);
        }

        .update{
            background-color: rgba(135, 75,160);
            color: white;
        }
	</style>

@endsection