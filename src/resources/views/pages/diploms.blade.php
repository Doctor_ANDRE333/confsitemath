@extends('layouts.AppLK')

@section('title')
	Дипломы и сертификаты
@endsection

@section('content')
<main class="page-content">
    <div class="container fon">
		@include('common.errors')
		
		@if (Session::has('message'))
			<div class = "alert alert-primary mt-3"> {{ Session::get('message') }} </div>
		@endif
		@if (Session::has('alert'))
			<div class = "alert alert-danger mt-3"> {{ Session::get('alert') }} </div>
		@endif
		
		<h3> Дипломы и сертификаты </h3>

		<hr>
		
		@if (Auth::user()->Diplom == '[]')
			<h5> Ваш сертификат ещё не создан :( </h5>
		@else
			<div class="row mt-3">
				<div class="col-6">
					<button class="btn btn download" type="submit" method="GET" onclick="location.href='{{ route('getDiplom', Auth::user()->id) }}'">Загрузить ваш сертификат</button>
				</div>
				<div class="col-6">
				</div>
			</div>
		@endif
    </div>
</main>

<style>
    body{
        background-image: url(http://pictures.std-1056.ist.mospolytech.ru/blackfon.jpg)
    }
	
	.fon{
		background-image: url(http://pictures.std-1056.ist.mospolytech.ru/whitefon.jpg);
		border-radius: 5px;
	}

	.download{
		background-color:  rgba(135, 75,160);
		color: white;
	}
</style>

@endsection