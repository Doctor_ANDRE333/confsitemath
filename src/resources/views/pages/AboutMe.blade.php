@extends('layouts.AppLK')

@section('title')
	Обо мне
@endsection

@section('content')

<main class="page-content">
    <div class="container">
		@include('common.errors')
		
		@if (Session::has('message'))
			<div class = "alert alert-primary mt-3"> {{ Session::get('message') }} </div>
		@endif

		<div class="card text-center" id="cardR">
			<div class="card-header">
				<h5 class="card-title">Изменение пользовательских данных</h5>
			</div>

			<div class="card-body">
			<form action="{{ route('UpdateMe', $user) }}" method="POST" class="form-horizontal mt-3" enctype="multipart/form-data">
			  {{ csrf_field() }}
				<div class="form-group">
					<div class="row">
						<div class="col-sm-6" style = "margin: 0 auto">
							<h5><label for="user" class="col control-label mb-0">Фамилия</label></h5>
						  	<input value="{{ $user->surname }}" type="text" name="surname" id="user-surname" class="form-control mb-3">
						</div>
					</div>
					
					<div class="row">
						<div class="col-sm-6" style = "margin: 0 auto">
							<h5><label for="user" class="col control-label mb-0">Имя</label></h5>
						  	<input value="{{ $user->name }}" type="text" name="name" id="user-name" class="form-control mb-3">
						</div>
					</div>

					<div class="row">
						<div class="col-sm-6" style = "margin: 0 auto">
							<h5><label for="user" class="col control-label mb-0">Отчество</label></h5>
						  	<input value="{{ $user->last_name }}" type="text" name="last_name" id="user-last_name" class="form-control mb-3">
						</div>
					</div>

					<div class="row">
						<div class="col-sm-6" style = "margin: 0 auto">
							<h5><label for="user" class="col control-label mb-0">Адрес</label></h5> <span style = "font-size: 18px">(страна, город)</span>
						  	<input value="{{ $user->address }}" type="text" name="address" id="user-address" class="form-control mb-3">
						</div>
					</div>

					<div class="row">
						<div class="col-sm-6" style = "margin: 0 auto">
							<h5><label for="user" class="col control-label mb-0">Место работы</label></h5>
						  	<input value="{{ $user->workplace }}" type="text" name="workplace" id="user-workplace" class="form-control mb-3">
						</div>
					</div>

					<div class="row">
						<div class="col-sm-6" style = "margin: 0 auto">
							<h5><label for="user" class="col control-label mb-0">Должность</label></h5> <span style = "font-size: 18px">(необязательно)</span>
						  	<input value="{{ $user->amplua }}" type="text" name="amplua" id="user-amplua" class="form-control mb-3">
						</div>
					</div>
										
					<div class="row" style = "display: flex; align-items: center; justify-content: center;">

						<div class="col-sm-6" >
							<h5><label for="user" class="col control-label mb-0">Дата рождения</label></h5>
						  	<input value="{{ $user->birth }}" type="date" name="birth" id="user-birth" class="form-control mb-3">
						</div>
					</div>

					<div class="row">
						<div class="col-sm-6" style = "margin: 0 auto">
							<h5><label for="user" class="col control-label mb-0">Телефон</label></h5>
						  	<input value="{{ $user->telephone }}" type="text" name="telephone" id="user-telephone" class="form-control mb-3">
						</div>
					</div>

					<div class="row">
						<div class="col-sm-6" style = "margin: 0 auto">
							<h5><label for="user" class="col control-label mb-0">Email</label></h5>
						  	<input value="{{ $user->email }}" type="text" name="email" id="user-email" class="form-control mb-3">
						</div>
					</div>

					<div class="row">
						<div class="col-sm-6" style = "margin: 0 auto">
							<h5><label for="user" class="col control-label mb-0">Сменить пароль?</label><h5>
							<select class = "form-control" id = "passCheck" name = "passCheck">
								<option value="0"> Нет </option>
								<option value="1"> Да </option>
							</select>
						</div>
					</div>

					<div class = "passwords">
						<!-- Не стирать, нужно! -->
					</div>

					<div class = "row">
						<div class="col-6" style = "margin: 0 auto">
							<button type="submit" class="btn mt-4 mb-2 form-control">
								<i class="fa fa-plus"></i> Редактировать 
							</button>
						</div>
					</div>
				</div>
			</form>
			</div>

			<div class="card-footer text-muted">
				Московский Политех
			</div>
		</div>
    </div>
</main>

<script>
	let Block = document.querySelector('.passwords');
	console.log(Block);

	function Drop(){
		let password_select = document.getElementById('passCheck').value;
		if (password_select != '1'){
			Block.innerHTML = '';
		}
		else if (password_select == '1' && Block.firstChild == null){
			let newDiv = document.createElement('div');
			newDiv.className = 'passwords';
			newDiv.innerHTML = `
			<div class="row">
				<div class="col-sm-6" style = "margin: 0 auto">
					<h5><label for="user" class="col control-label mb-0">Пароль</label><h5>
					<input value="" type="password" placeholder="Для изменения введите новый или старый пароль" name="password" id="user-password" class="form-control mb-3">
				</div>
			</div>
			
			<div class="row">
				<div class="col-sm-6" style = "margin: 0 auto">
					<h5><label for="user" class="col control-label mb-0">Подтвердите пароль</label><h5>
					<input value="" type="password" placeholder="Подтвердите пароль" name="password_confirmation" id="user-password-confirmation" class="form-control mb-3">
				</div>
			</div>
			`
			console.log(newDiv);
			Block.append(newDiv);
			}
		}
	
	setInterval(Drop, 250);
</script>

<style>
    body{
        background-image: url(http://pictures.std-1056.ist.mospolytech.ru/blackfon.jpg)
    }

	#cardR{
		background-image: url(http://pictures.std-1056.ist.mospolytech.ru/whitefon.jpg)
	}

	.card-header h5{
		color:  rgba(135, 75,160)
	}

	.btn{
		background-color:  rgba(135, 75,160);
		color: white;
	}
</style>
@endsection