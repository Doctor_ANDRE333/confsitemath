@extends('layouts.app1')

@section('title')
	Фотоархив
@endsection

@section('content')

<!-- fancyBox CSS -->
<link href="http://pictures.std-1056.ist.mospolytech.ru/jquery.fancybox.min.css" rel="stylesheet">
<link href="http://pictures.std-1056.ist.mospolytech.ru/jquery.fancybox.css" rel="stylesheet">

    <style>
    .thumb img {
        -webkit-filter: grayscale(0);
        filter: none;
        border-radius: 5px;
        background-color: #fff;
        border: 1px solid #ddd;
		transition: all 1s;

        
        padding: 5px;
    }

    .thumb img:hover {
        -webkit-filter: grayscale(1);
        filter: grayscale(1);
    }

    .thumb {
        padding: 5px;
    }

    body{
        background-image: url(http://70b0847f967d1d37694c-789248f0448453b835406008213ad4a2.r86.cf2.rackcdn.com/cover_photo_311239_1464547715.jpg)
    }
</style>

<div class="container-fluid mb-2">
    <div class="row">
        <div class = "col-12">
            <h1 class = "text-center" style="margin-top: 100px; background-image: url(https://wallup.net/wp-content/uploads/2017/11/23/433673-minimalism-low_poly.jpg); border-radius: 20px"> Фотоархив конференции </h1>
        </div>
    </div>
</div>

<div class="container">
    <div class="row mb-3">
        <div class="col-lg-3 col-md-4 col-6 thumb">
            <a data-fancybox="gallery" href="http://pictures.std-1056.ist.mospolytech.ru/confphotos/1.jpg">
                <img class="img-fluid" src="http://pictures.std-1056.ist.mospolytech.ru/confphotos/1.jpg" alt="">
            </a>
        </div>
		
        <div class="col-lg-3 col-md-4 col-6 thumb">
            <a data-fancybox="gallery" href="http://pictures.std-1056.ist.mospolytech.ru/confphotos/2.jpg">
                <img class="img-fluid" src="http://pictures.std-1056.ist.mospolytech.ru/confphotos/2.jpg" alt="">
            </a>
        </div>
		
		<div class="col-lg-3 col-md-4 col-6 thumb">
            <a data-fancybox="gallery" href="http://pictures.std-1056.ist.mospolytech.ru/confphotos/3.jpg">
                <img class="img-fluid" src="http://pictures.std-1056.ist.mospolytech.ru/confphotos/3.jpg" alt="">
            </a>
        </div>
		
		<div class="col-lg-3 col-md-4 col-6 thumb">
            <a data-fancybox="gallery" href="http://pictures.std-1056.ist.mospolytech.ru/confphotos/4.jpg">
                <img class="img-fluid" src="http://pictures.std-1056.ist.mospolytech.ru/confphotos/4.jpg" alt="">
            </a>
        </div>
		
		<div class="col-lg-3 col-md-4 col-6 thumb">
            <a data-fancybox="gallery" href="http://pictures.std-1056.ist.mospolytech.ru/confphotos/5.jpg">
                <img class="img-fluid" src="http://pictures.std-1056.ist.mospolytech.ru/confphotos/5.jpg" alt="">
            </a>
        </div>
		
		<div class="col-lg-3 col-md-4 col-6 thumb">
            <a data-fancybox="gallery" href="http://pictures.std-1056.ist.mospolytech.ru/confphotos/6.jpg">
                <img class="img-fluid" src="hhttp://pictures.std-1056.ist.mospolytech.ru/confphotos/6.jpg" alt="">
            </a>
        </div>
    </div>
</div>

<!-- jQuery -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<!-- Popper -->
<script src="pacanizakintekudanado/popper.js"></script>
<!-- fancyBox JS -->
<script src="http://pictures.std-1056.ist.mospolytech.ru/jquery.fancybox.min.js"></script>
<script src="http://pictures.std-1056.ist.mospolytech.ru/jquery.fancybox.js"></script>
<!-- Gridify -->
<script src="http://pictures.std-1056.ist.mospolytech.ru/gridify.js"></script>
<script>
    $(function () {
        var options = {
            srcNode: 'img',             // grid items (class, node)
            margin: '15px',             // margin in pixel, default: 0px
            width: '240px',             // grid item width in pixel, default: 220px
            max_width: '',              // dynamic gird item width if specified, (pixel)
            resizable: true,            // re-layout if window resize
            transition: 'all 1s ease' // support transition for CSS3, default: all 0.5s ease
        };
        $('.grid').gridify(options);
    });
</script>

@endsection