@extends('layouts.AppLK')

@section('title')
	Добавление работы
@endsection

@section('content')
	<div class = "container" style = "min-height: 700px; margin-top: 100px">
			<div class="text-center ml-5 mr-5">
			@include('common.errors')
			<div>
        <div class="card text-dark bg-light mt-5" style = "width: 75%; margin: 0 auto">
            <div class="card-header">
                <h5 class = "text-center title"> Добавление работы </h5>
            </div>

            <div class="card-body"  id="cardW">

                <form action="{{ route('works.store') }}" method="POST" class="form-horizontal mt-3" enctype="multipart/form-data">
                    {{ csrf_field() }}

                    <div class = "row">
						<div class = "col-12">
                            <h5><b> Автор: </b></h5>
							<h5><p> {{ $user->surname }}&nbsp;{{ $user->name }} </p></h5>
                        </div>

                        <div class = "col-12 mt-2">
                            <h5><b> Тема: </b></h5>
                            <input type = "text" name = "topic" value = "{{old('topic')}}" class = "form-control">
                        </div>

                        <div class = "col-12 mt-2">
                            <h5><b> Название работы: </b></h5>
                            <input type = "text" name = "label" value = "{{old('label')}}" class = "form-control">
                        </div>

                        <div class = "col-12 mt-2">
                            <h5><b> Название работы (English): </b></h5><span class="dop">(необязательно)</span> 
                            <input type = "text" name = "label_en" value = "{{old('label_en')}}" class = "form-control">
                        </div>

                        <div class = "col-12 mt-2">
                            <h5><b> Форма участия: </b></h5><span class="dop">(очная, заочная, очно-заочная)</span> 
                            <input type = "text" name = "part_form" value = "{{old('part_form')}}" class = "form-control">
                        </div>

                        <div class = "col-6 mt-2">
                            <h5><b> Ученая степень: </b></h5><span class="dop">(необязательно)</span>
                            <input type = "text" name = "academic_degree" value = "{{old('academic_degree')}}" class = "form-control">
                        </div>

                        <div class = "col-6 mt-2">
                            <h5><b> Ученое звание: </b></h5><span class="dop">(необязательно)</span> 
                            <input type = "text" name = "academic_label" value = "{{old('academic_label')}}" class = "form-control">
                        </div>

                        <div class = "col-12 mt-2">
                            <h5><b> Номер секции: </b></h5>
                             <input type = "text" name = "section_number" value = "{{old('section_number')}}" class = "form-control">
                        </div>

						<div class = "col-12 mt-2">
                            <h5><b> Соавторы: </b></h5><span class="dop">(необязательно)</span>
                             <input type = "text" name = "collaborator" value = "{{old('collaborator')}}"  class = "form-control">
                        </div>
						<div class = "col-12 mt-2">
							<h5><b> Документ (загружайте только в формате .docx): </b></h5>
							<input type="file" name="file" value = "{{old('file')}}" class="form-control mb-3 ">
						</div>
                    </div>
                        <button type = "submit" name = "Sub" value = "Добавить работу" class = "form-control mt-3 btn create">
                            <i class="fas fa-plus"></i> Загрузить
                        </button>
                </form>
            </div>
        </div>
	</div>
	
	<style>
		body{
        background-image: url(http://pictures.std-1056.ist.mospolytech.ru/blackfon.jpg)
    }

	#cardW{
		background-image: url(http://pictures.std-1056.ist.mospolytech.ru/whitefon.jpg)
	}
	
	.dop{
		font-size: 18px;
	}

    .create{
        background-color:  rgba(135, 75,160);
		color: white;
    }

    .title{
        color:  rgba(135, 75,160);
    }
	</style>

@endsection