@extends('layouts.app1')

@section('title')
	Главная страница
@endsection

@section('content')
    @if (Session::has('warning'))
        <div class = "container pl-0 pr-0">
            <div class = "alert alert-danger" style = "margin-top: 100px;"> {{ Session::get('warning') }} </div>
        </div>

        <div class = "container rounded" style = "margin-top: 20px; background-color: WhiteSmoke">
    @else
        <div class = "container rounded" style = "margin-top: 100px; background-color: WhiteSmoke">
    @endif

        <div class = "row">
            <div class = "col-12">
                <h2 class = "text-justify"> Всероссийская научно-практическая конференция «Математика: теоретические и прикладные исследования» </h2>
            </div>
        </div>
        <div class = "row mt-2">
            <div class = "col-md-6 col-12">
                <p class = "text-justify myText"> 
                    <h3><strong>Добро пожаловать на сайт Всероссийской конференции по математике!</strong></h3>
                    Наша конференция стремится достичь следующих целей и выполнить следующие задачи:
                    <br>
                    — Обмен информацией о результатах исследований, проводимых российскими научными школами по современным проблемам высшей, прикладной и вычислительной математики, по математическому моделированию прикладных задач в естественнонаучных областях, в технике и технологии, в экономике, по новейшим образовательным технологиям.
                    <br><br> 
                    — Координация исследований в рассматриваемых областях.
                    <br><br>
                    — Привлечение молодых ученых и студентов к работе над актуальными проблемами в этих областях.
                </p>
            </div>

            <div class = "col-md-6 col-12">
                <img class = "mt-2 p-1 rounded mb-1" src = "http://pictures.std-1056.ist.mospolytech.ru/conf.jpg" width = "100%" style = "border: 2px solid rgba(135, 75, 160);">
            </div>
        </div>
    </div>

    <div class = "container-fluid mt-4 pt-4" style="background-color: whitesmoke; min-height: 430px">
        <div class = "row">
			
            <div class = "col-md-4 col-12 mb-2 hover-zoom" onClick="document.location='/requirements'">
                <div class="card p-2">
                    <img class="card-img-top" src="http://pictures.std-1056.ist.mospolytech.ru/p2.jpg" alt="Card image cap">

                    <div class="card-body">
                        <h5 class = "text-center">
                            <a> Требования к статьям </a>
                        </h5>
                    </div>
                </div>
            </div>
			
            <div class = "col-md-4 col-12 mb-2 hover-zoom" onClick="document.location='/photos'">
                <div class="card p-2">
                    <img class="card-img-top" src="http://pictures.std-1056.ist.mospolytech.ru/p1.jpg" alt="Card image cap">

                    <div class="card-body">
                        <h5 class = "text-center">
                            <a> Посмотреть фотоархив </a>
                        </h5>
                    </div>
                </div>
            </div>

            <div class = "col-md-4 col-12 mb-3 hover-zoom" onClick="document.location='/rasp'">
                <div class="card p-2">
                    <img class="card-img-top" src="http://pictures.std-1056.ist.mospolytech.ru/p3.jpg" alt="Card image cap">

                    <div class="card-body">
                        <h5 class = "text-center">
                            <a> Расписание </a>
                        </h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
<style>
    body{
        background: url(http://pictures.std-1056.ist.mospolytech.ru/bg.jpg);
    }
	
	.hover-zoom{
        -webkit-transform: scale(1);
        transform: scale(1);
        -webkit-transition: .3s ease-in-out;
        transition: .3s ease-in-out;
    }

	.hover-zoom:hover{
        -webkit-transform: scale(1.06);
        transform: scale(1.06);
        transition: 0.7s ease-in-out;
    }

    .myText{
        font-size: 19px;
    }

</style>

@endsection