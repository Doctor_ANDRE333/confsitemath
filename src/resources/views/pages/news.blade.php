@extends('layouts.app1')

@section('title')
	Новости
@endsection

@section('content')
	<div class="container-fluid mb-3">
		<div class="row">
			<div class = "col-12">
				<h1 class = "text-center" style="margin-top: 100px; background-image: url(https://wallup.net/wp-content/uploads/2017/11/23/433673-minimalism-low_poly.jpg); border-radius: 20px"> Новостная лента <span class="badge badge-dark badge-pill"> {{ count($news) }} </span> </h1>
			</div>
		</div>
	</div>
    
    <div class="container" style = "text-align: center; min-height: 650px">
        @if (Session::has('message'))
            <div class = "alert alert-primary mt-3"> {{ Session::get('message') }} </div>
        @endif

        @if (Auth::user() && Auth::user()->role == 'redactor')
            <a class = "btn btn create form-control mb-2" style = "font-size: 24px" href = "{{ route('news.create') }}"> Добавить новость </a>
        @endif

        @if (count($news) > 0)
            @foreach ($news as $currNews)
                <div class = "row myNews rounded-top" style = "margin-left: 0px; margin-right: 0px">
                    <div class = "col-6">
                        <h3 class = "text-left">{{ $currNews->label }}</h3>
                    </div>

                    <div class = "col-6" style = "text-align: right;">
                        @if (Auth::user() && Auth::user()->role == 'redactor')
                            <a class = "btn p-0">
                                <svg width="2em" height="2em" viewBox="0 0 16 16" class="bi bi-pencil-square mr-2" fill="currentColor" xmlns="http://www.w3.org/2000/svg" onclick="location.href='{{route('news.edit', $currNews->id)}}'">
                                    <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456l-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"></path>
                                    <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"></path>
                                </svg>
                            </a>
                            
                            <form action = "{{ route('news.destroy', $currNews->id) }}" method = "POST" style = "display: inline">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                                
                                <button type = "submit" class = "p-0">
                                    <svg width="2em" height="2em" viewBox="0 0 16 16" class="bi bi-trash" fill="currentColor" xmlns="http://www.w3.org/2000/svg" style = "margin-right: 8px">
                                        <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"></path>
                                        <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4L4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"></path>
                                    </svg>
                                </button>
                            </form>
                        @endif
                    </div>
                </div>

                <div class = "row myNews mb-3 rounded-bottom" style = "margin-left: 0px; margin-right: 0px">
                    <div class = "col-md-6 col-12">
                        <img style = "display: block; margin: auto" src = "{{ $currNews->img }}" width = "100%">
                    </div>
                        
                    <div class = "col-md-6 col-12">
                        <div style = "max-height: 230px; overflow-y: auto;">
                            <p class = "text-justify">{{ $currNews->text }}</p>
                        </div>

                        <hr>
                        <p class = "date text-right">{{ date('d.m.Y H:i:s', strtotime("+3 hours", strtotime($currNews->created_at))) }}</p>
                    </div>
                </div>
            @endforeach     
        @else
            Новостей нет
        @endif
    </div>
    
<style>
    body{
        background-image: url(http://pictures.std-1056.ist.mospolytech.ru/blackfon.jpg)
    }

    .myNews{
        border: 1px solid black;
        padding: 25px;
		background-image: url(http://pictures.std-1056.ist.mospolytech.ru/whitefon.jpg)
    }

    h3{
        color:  rgba(135, 75,160)
    }

    form > button{
		background-color: Transparent;
		background-repeat:no-repeat;
		border: none;
		cursor:pointer;
		overflow: hidden;
		margin-top: 6px;
		
	}

    .date{
        color:  rgba(135, 75,160);
        font-size: 16px;
    }

    .create{
        background-color:  rgba(135, 75,160);
        color: white;
    }
	
</style>
@endsection