@extends('layouts.app1')

@section('title')
	Архив предыдущих конференций
@endsection

@section('content')
<style>
    body{
        background-image: url(http://pictures.std-1056.ist.mospolytech.ru/blackfon.jpg)
    }
	
	.teacher{
		color: white
	}

	h3{
        padding: 10px;
		color:  rgba(135, 75,160)
    }

	.maininfo{
		background-image: url(http://pictures.std-1056.ist.mospolytech.ru/whitefon.jpg)
	}
</style>

<div class="container-fluid mb-3">
    <div class="row">
        <div class = "col-12">
            <h1 class = "text-center" style="margin-top: 100px; background-image: url(https://wallup.net/wp-content/uploads/2017/11/23/433673-minimalism-low_poly.jpg); border-radius: 20px"> Предыдущие конференции </h1>
        </div>
    </div>
</div>
 @foreach ($histories as $history)
<div class = "container rounded maininfo mb-3">
    <div class = "row">
        <div class = "col-12">
            <h1>{{ $history->name }}</h1>
        </div>
    </div>

    <div class = "row mt-2">
        <div class = "col-md-6 col-12">
        <h3>{{ $history->datePR }}</h3>
            <p class = "text-justify"> 
			{{ $history->text }}
            </p>
        </div>

        <div class = "col-md-6 col-12">
            <img style = "display: block; margin: auto" src = "{{ $history->img }}" width = "100%">
        </div>
    </div>
</div>
@endforeach



