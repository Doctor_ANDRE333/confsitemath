<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title> @yield('title') </title>

    <!-- Styles -->
    <link href="/css/app.css" rel="stylesheet">
    <link href="http://pictures.std-1056.ist.mospolytech.ru/fonts/fonts.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css">
	<!-- Icons font CSS-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.10.2/css/all.css">
	<link rel="shortcut icon" href="http://pictures.std-1056.ist.mospolytech.ru/polytech_icon.png" type="image/png"> 
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Montserrat+Alternates:wght@300;400&display=swap" rel="stylesheet">
    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
<body>
    <div id = "app">
        <div class = "fixed-top">
            <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarText">
                    <div class="header-page_start">
                        <div class="logo">
                            <img class="logo_img" src="http://pictures.std-1056.ist.mospolytech.ru/polytech_logo.png" alt="logo" width="220" height="59">
                        </div>
                    </div>

                    <ul class="navbar-nav mr-auto ml-5">
                        <li class="header-page_li">
                            <a class="header-page_link" href="{{ route('Welcome') }}">
                                <span class="header-page_text">Главная</span>
                            </a>
                        </li>

                        <li class="header-page_li">
                            <a class="header-page_link" href="{{  route('ConfNews') }}">
                                <span class="header-page_text">Новости</span>
                            </a>
                        </li>

                        <li class="header-page_li">
                            <a class="header-page_link" href="{{ route('Info') }}">
                                <span class="header-page_text">Личный кабинет</span>
                            </a>
                        </li>
						
						<li class="header-page_li">
                            <a class="header-page_link" href="{{ route('Comitet') }}">
                                <span class="header-page_text">Орг. Комитет</span>
                            </a>
                        </li>
						
						<li class="header-page_li">
                            <a class="header-page_link" href="{{ route('Contacts') }}">
                                <span class="header-page_text">Контакты</span>
                            </a>
                        </li>

                        <li class="header-page_li">
                            <div class="dropdown">
                                <a class="header-page_link dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <span class = "header-page_text"> Ещё </span>
                                </a>

                                <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                    <a class="dropdown-item" href="{{ route('aboutConf') }}">О конференции</a>
                                    <a class="dropdown-item" href="{{ route('Rasp') }}">Расписание</a>
                                    <a class="dropdown-item" href="{{ route('Requirements') }}">Требования к статьям</a>
                                    <a class="dropdown-item" href="{{ route('Archive') }}">Архив конференций</a>
                                    <a class="dropdown-item" href="{{ route('Photos') }}">Фотоархив</a>
                                </div>
                            </div>
                        </li>
                    </ul>

                    <ul class="nav navbar-nav navbar-right">
                        @if (Auth::guest())
							<div style = "text-decoration: none;">
								<li class = "inline"><a class="nav-link" href="{{ url('/login') }}">Войти</a></li>
								<li class = "inline"><a class="nav-link" href="{{ url('/register') }}">Зарегистрироваться</a></li>
							</div>
                        @else
                            <div class="dropleft">
                                <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    {{ Auth::user()->name }}
                                </button>
                                <div class="dropdown-menu">
                                    <a class= "dropdown-item" href="{{ url('/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                        Выйти
                                    </a>

                                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </div>
                            </div>
                        @endif
                    </ul>
                </div>
            </nav>
        </div>

        @yield('content')
    </div>

    <footer class="footer bg-dark">
        <div class="container">
            <div class = "row">
                <div class = "col-12 labelCont">
                    <h4> Контакты </h4>
                    <hr>
                </div>

                <div class = "col-12 contactInfo">
                    <div>
                        <i class="fas fa-phone-square-alt"></i><p>  Тел: +7 (495) 223-05-23-30  </p>
                    </div>

                    <div>
                        <i class="fas fa-map-marker-alt"></i> <p>  Адрес: г. Москва, ул. Павла Корчагина, д.22, ауд. ПК441  </p>
                    </div> 

                    <div>
                        <i class="fas fa-mail-bulk"></i> <p>  km@mospolytech.ru  </p>
                    </div>
                </div>
            </div>
        </div>
    </footer>

	<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
	<!--<script src="https://code.jquery.com/jquery.js"></script>
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
    <script src="/js/app.js"></script>-->
	    <!-- Jquery JS-->
    <script src="http://pictures.std-1056.ist.mospolytech.ru/registrasion/jquery.min.js"></script>
    <!-- Vendor JS-->
    <script src="http://pictures.std-1056.ist.mospolytech.ru/registrasion/select2.min.js"></script>
    <script src="http://pictures.std-1056.ist.mospolytech.ru/registrasion/moment.min.js"></script>
    <script src="http://pictures.std-1056.ist.mospolytech.ru/registrasion/daterangepicker.js"></script>

    <!-- Main JS-->
    <script src="http://pictures.std-1056.ist.mospolytech.ru/registrasion/global.js"></script>
	<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-23581568-13');
	</script>
	<script defer src="https://static.cloudflareinsights.com/beacon.min.js" data-cf-beacon='{"si":10,"rayId":"640f95bbfb703a8f","version":"2021.4.0"}'></script>

	<style>
		.inline{
			display:inline-block;
		}
	
        .contactInfo p{
            display: inline;
        }

        .fas{
            color: white;
        }
	
        .footer p{
            color: white;
        }

        * {
            margin: 0;
            padding: 0;
            outline: none;
        }

        body, html {
            width: 100%;
            height: 100%;
			font-size: 0.95rem;
        }

        .labelCont{
            color: white;
        }

        body{
            background-color: #ffffff;
			font-family: "Montserrat Alternates";
			font-style: "Regular";
        }

        .header-page_container{
            padding: 0 10px;
            width: 100%;
            margin: 0 auto;
            display: flex;
            justify-content: space-between;
            align-items: center;
        }

        .header-page {
            top: 0;
            left: 0;
            width: 100%;
            z-index: 5;
            background: rgba(13, 2, 17, 0.911);
            overflow: hidden;
        }

        .header-page_start {
            padding: 4px;
            padding-left: 20px;
        }

        .header-page_end {
            display: flex;
            align-items: center;
            padding-right: 30px;
        }

        .header-page_ul {
            list-style: none;
            margin: 0;
            padding: 0;
            display: flex;
        }

        .header-page_link {
            text-decoration: none;
            display: block;
            padding: 15px 30px;
            font-size: 18px;
            font-weight: 900;
            position: relative;
            transition: color 2s;
            color: #ffffff;
			
        }

        .header-page_text {
            position: relative;
        }

        .header-page_link:hover::before{
            color: #ffffff;
            opacity: 1;
        }

        .header-page_link:hover{
            color: #000000;
        }

        footer {
            padding: 20px;
            text-align: center;
            clear:both;
            margin: auto;
            align-content: center;
        }

        h1,h2{
            padding: 10px;
            color:  rgba(135, 75,160);

        }

        .card h5{
            padding: 10px;
            color:  rgba(135, 75,160)
        }

        @media (min-width: 300px) {
            .header-page_link{
                padding: 12px 25px;
                font-size: 16px;
            }

            .header-page_link::before{
                content: '';
                position: absolute;
                top: 50%;
                left: 0;
                background: #ffffff;
                width: 100%;
                height: calc(100% + 40px);
                transform: translateY(-50%);
                opacity: 0;
                transition: all 2s;
            }
        }

        @media (max-width:900px)  {
            .header-page_link{
                padding: 12px 10px;
                font-size: 12px;
            }

            .header-page_link::before{
                content: '';
                position: absolute;
                top: 50%;
                left: 0;
                background: #ffffff;
                width: 100%;
                height: calc(90%);
                transform: translateY(-50%);
                opacity: 0;
                transition: all 2s;
            }
        }
    </style>
</body>
</html>
