<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title> @yield('title') </title>

    <!-- Styles -->
    <link href="/css/app.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo asset('css/test.css')?>" type="text/css"> 
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css">

    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
<body>
    <div class = "fixed-top">
        <header class="header-page">
            <div class="header-page_container">
                <div class="header-page_start">
                    <div class="logo">
                        <img class="logo_img" src="http://pictures.std-1056.ist.mospolytech.ru/polytech_logo.png" alt="logo" width="220" height="59">
                    </div>
                </div>
                <div class="header-page_end">
                    <nav class="header-page_nav" >
                        <ul class="header-page_ul">
                            <li class="header-page_li">
                                <a class="header-page_link" href="#">
                                    <span class="header-page_text">Главная</span>
                                </a>
                            </li>

                            <li class="header-page_li">
                                <a class="header-page_link" href="#">
                                    <span class="header-page_text">Новости</span>
                                </a>
                            </li>

                            <li class="header-page_li">
                                <a class="header-page_link" href="#">
                                    <span class="header-page_text">Личный кабинет</span>
                                </a>
                            </li>

                            <li class="header-page_li">
                                <a class="header-page_link" href="#">
                                    <span class="header-page_text">Контакты</span>
                                </a>
                            </li>
                            
                            <li class="header-page_li">
                                <div class="dropdown">
                                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Dropdown
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                                    <button class="dropdown-item" type="button">Action</button>
                                    <button class="dropdown-item" type="button">Another action</button>
                                    <button class="dropdown-item" type="button">Something else here</button>
                                </div>
                                </div>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </header>
    </div>

    @yield('content')

    <footer >
        <p>© Московский политех </p>
        <p>Адрес: г. Москва, ул. Павла Корчагина, д.22, ауд. ПК441</p>
        <p>Тел: 8 (495) 223-05-23 доб.3140</p>
    </footer>

    <!-- Scripts -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
	<!--<script src="https://code.jquery.com/jquery.js"></script>
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
    <script src="/js/app.js"></script>-->
</body>

</html>


<style>
    * {
        margin: 0;
        padding: 0;
        outline: none;
    }

    body, html {
        width: 100%;
        height: 100%;
    }

    body{
        background-color: #ffffff;
        font-family: 'Gilroy-Regular',Verdana;
    }

    .header-page_container{
        padding: 0 10px;
        width: 100%;
        margin: 0 auto;
        display: flex;
        justify-content: space-between;
        align-items: center;
    }

    .header-page {
        top: 0;
        left: 0;
        width: 100%;
        z-index: 5;
        background: rgba(13, 2, 17, 0.911);
        overflow: hidden;
    }

    .header-page_start {
        padding: 4px;
        padding-left: 20px;
    }

    .header-page_end {
        display: flex;
        align-items: center;
        padding-right: 30px;
    }

    .header-page_ul {
        list-style: none;
        margin: 0;
        padding: 0;
        display: flex;
    }

    .header-page_link {
        text-decoration: none;
        display: block;
        padding: 15px 30px;
        font-family: 'Gilroy-Regular',Verdana;
        font-size: 18px;
        font-weight: 900;
        position: relative;
        transition: color 2s;
        color: #ffffff;
    }

    .header-page_link::before{
        content: '';
        position: absolute;
        top: 50%;
        left: 0;
        background: #ffffff;
        width: 100%;
        height: calc(100% + 40px);
        transform: translateY(-50%);
        opacity: 0;
        transition: all 2s;
    }

    .header-page_text {
        position: relative;
    }

    .header-page_link:hover::before{
        color: #ffffff;
        opacity: 1;
    }

    .header-page_link:hover{
        color: #000000;
    }

    footer {
        padding: 20px;
        text-align: center;
        clear:both;
        margin: auto;
        align-content: center;
        font-family: 'Gilroy-Regular',Verdana;		
    }

    @media (min-width: 300px) {
        .header-page_link{
        padding: 12px 25px;
        font-size: 14px;
    }

    @media (max-width:900px)  {
        .header-page_link{
        padding: 12px 10px;
        font-size: 10px;
    }
</style>
