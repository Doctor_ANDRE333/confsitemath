<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;
use App\Comment;
use App\User;
use App\ConfNew;
use App\Report;
use App\Answer;
use App\Diplom;
use App\History;

class DownloadController extends Controller
{
   public function getDiplom($id)
	{
		if (Auth::user()->role == 'default')
		{
			$diplom = Diplom::where('user_id','=',$id)->get();
			$user = User::find($id);
			$filename = $user->surname . "_" . $user->name . "_" . $user->birth . "_token:" . $user->id . ".png";
			$location = public_path() . '/Diploms/' . $filename;
	
		return response()->download($location, $filename);
			
		}
		else
		{
		$diplom = Diplom::find($id);	
        $filename = $diplom->User->surname . "_" . $diplom->User->name . "_" . $diplom->User->birth . "_token:" . $diplom->User->id . ".png";
		$location = public_path() . '/Diploms/' . $filename;
	
		return response()->download($location, $filename);
		}
    }
	
	public function getFile($id){
		$report = Report::find($id);
        $filename = $report->User->surname . "_" . $report->User->name . "_" . $report->User->birth . "_token:" . $report->User->id .  ".docx";
	//	  dd($filename);
		$location = public_path() . '/Reports/' . $filename;

		return response()->download($location, $filename);
    }
	
	public function getOsnova(){
		 $filename = "Основные положения.docx";
		$location = public_path() . "/poloshenia.docx";
		return response()->download($location, $filename);
	}
	
	public function getPrimer(){
		 $filename = "Пример оформления статьи.docx";
		$location = public_path() . "/primerStati.docx";
		return response()->download($location, $filename);
	}
}
