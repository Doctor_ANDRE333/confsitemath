<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;
use App\Comment;
use App\User;
use App\ConfNew;
use App\Report;
use App\Answer;
use App\Diplom;
use App\History;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
   //     $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexHome(){
        return view('pages.welcome');
    }

    public function indexRasp(){
        return view('pages.rasp');
    }
	
    public function indexContacts(){
        return view('pages.contacts');
    }

    public function indexPhotos(){
        return view('pages.photos');
    }

    public function indexNews(){
        $news = ConfNew::all();
        return view('pages.news', ['news' => $news]);
    }
	
	public function indexComitet(){
       return view('pages.comitet');
    }
	
	public function indexArchive(){
		$histories = History::all();
        return view('pages.archive', ['histories' => $histories]);
    }
	

    public function indexRequirements(){
        return view('pages.requirements');
    }

    public function indexConf(){
        return view('pages.aboutconf');
    }
	
	public function indexUpload(){
        return view('pages.uploadWork');
    }
	
    public function AboutMe(){
        if (Auth::user()){
			$user = Auth::user();
            return view('pages.AboutMe', ['user'=>$user]);
        } else {
            return view('auth.login');
        }
    }

    public function WorkStatus()
	{	
		if (Auth::user()->role == 'default')
		{
			$reports = Auth::user()->Report;
			return view('pages.WorkStatus', ['reports'=>$reports]);
		}
		else
		{
			$reports = Report::All();
			return view('pages.WorkStatusComitet', ['reports'=>$reports]);
		}	
    }
	
	public function updateMe(Request $request, $id)
    {
		$this->validate($request, [
			'name' => 'required|max:255',
			'email' => 'required|email|max:255',
            'birth' => 'required',
            'surname' => 'required|max:25',
            'address' => 'required|max:55',
            'workplace' => 'required|max:55',
            'telephone' => 'required|numeric|max:89999999999'
		]);

        if (count(User::where('email', '=', $request->email)->where('id', '<>', $id)->get()) != 0){ 
            session()->flash('message', 'Данный email уже используется!');
            return redirect()->route('AboutMe', $id);
        }
        else{
            $i = User::find($id);
            
            $i->name = $request->name;
            $i->surname = $request->surname;
            $i->birth = $request->birth;
            $i->email = $request->email;
            $i->last_name = $request->last_name;
            $i->address = $request->address;
            $i->workplace = $request->workplace;
            $i->amplua = $request->amplua;
            $i->telephone = $request->telephone;

            if ($request->passCheck == 1){
                $this->validate($request, [
					'password' => 'required|min:6|confirmed'
				]);
                
                $i->password = bcrypt($request->password);
            }
	/*   
    */
            $i->save();
            session()->flash('message', 'Личные данные успешно обновлены!');
            return redirect('/lk/AboutMe');
        }
    }
	
	public function indexComitetComments($id){
        if (Auth::user()->role == 'comitet'){
           $comment = Comment::find($id);
           $ThisComment = $comment;
           $comments = collect();
           $comments = $comment->Answers;
           $comments->push($comment);
           $comments = $comments->sortBy('created_at');
           return view('pages.comments', ['comments' => $comments, 'ThisComment' => $ThisComment]);
        }
		else 
		{
			 session()->flash('warning', 'Вы не являетесь членом Орг.Комитета!');
			return redirect('/');
		}
    }
	
	public function indexDiploms(){
		  if (Auth::user()->role == 'comitet')
		  {
			$users = User::where('role','=','default')->get();	
			$diploms = Diplom::All();
			return view('pages.diplomsComitet', ['users' => $users, 'diploms' => $diploms]);
		  }
		  else 
		  {
			  return view('pages.diploms');
		  }
    }

    public function sendAnswer(Request $request, $comment_id){
        $this->validate($request, [
			'text' => 'required|max:255',
		]);
		$answer = new Answer;
		
        $answer->text = $request->text;
		$answer->user_id = $request->user_id;
        $answer->comment_id = $comment_id;
		
		$answer->save();
		session()->flash('message', 'Новый ответ успешно добавлен!');
		
		return redirect()->route('comments.index');
    }
	
	
    public function editStatus(Request $request, $id){
        $report = Report::find($id);

        //$arr = ['На рассмотрении', 'Работа одобрена', 'Работа отклонена'];
		//unset($arr[array_search("$report->status", $arr)]);
		
        $report->status = $request->status;
		$report->save();
		return redirect()->route('WorkStatus');
    }
	
	
	public function makeDiplom(Request $request)  
    {  
		$user = User::find($request->user);
		if (count($user->Diplom) > 0)
		{
			session()->flash('alert', 'Для этого участника уже создан сертификат!');
			return redirect()->route('Diploms');
			
		}
		else
		{
			$write = $user->name . " " . $user->surname;
		   $img = Image::make(public_path('/example.jpg'));  
		   $img->text($write, 2300, 1250, function($font) {  
			  $font->file(public_path('fonts/Radomir Tinkov - Gilroy-Black.ttf'));  
			  $font->size(130);  
			  $font->color('#000000');  
			  $font->align('center');  
			  $font->valign('bottom');  
			  $font->angle(0);  
		  });  
	  
		  $filename = $user->surname . "_" . $user->name . "_" . $user->birth . "_token:" . $user->id . ".png";
		  
		   $img->save(public_path('Diploms/' . $filename));
		   
		   
			$rep = Report::where('user_id','=',$user->id)->get()->first();	
		  $write2 = "Выступил(а) с докладом на тему: \n" . $rep->label;
		   $img2 = Image::make(public_path('Diploms/' . $filename));

		   $img2->text($write2, 2300, 1550, function($wr) {  
			  $wr->file(public_path('fonts/Radomir_Tinkov-Gilroy_SemiBold.ttf'));  
			  $wr->size(100);  
			  $wr->color('#000000');  
			  $wr->align('center');  
			  $wr->valign('bottom');  
			  $wr->angle(0);  
		  });  
		  
		  $img2->save(public_path('Diploms/' . $filename));
		  	
		  $write3 = "Моргунов Ю.А.,\nк.т.н, декан факультета\nбазовых компетенций";
		   $img3 = Image::make(public_path('Diploms/' . $filename));

		   $img3->text($write3, 2800, 2200, function($wr) {  
			  $wr->file(public_path('fonts/Radomir_Tinkov-Gilroy_SemiBold.ttf'));  
			  $wr->size(70);  
			  $wr->color('#000000');  
			  $wr->align('center');  
			  $wr->valign('bottom');  
			  $wr->angle(0);  
		  });  
		  
		  $img3->save(public_path('Diploms/' . $filename));
		  
			$diplom = new Diplom;
			$diplom->filename = $filename;
			$diplom->user_id = $user->id;
			
			$diplom->save();
			
			$for = "Сертификат для " . $user->surname . " " . $user->name . " создан!";
			session()->flash('message', $for);
			
			return redirect()->route('Diploms');
		}
	}
}
