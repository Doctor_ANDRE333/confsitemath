<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ConfNew;

class NewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.createNews');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'label' => 'required|min:1|max:50',
			'text' => 'required|max:1024',
			'img' => 'required|max:255'
		]);
		
		$currNews = new ConfNew;
		$currNews->label = $request->label;
        $currNews->text = $request->text;
		$currNews->img = $request->img;
		
		$currNews->save();
		session()->flash('message', 'Новая новость успешно добавлена!');
		
		return redirect()->route('ConfNews');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $currNews = ConfNew::find($id);
        return view('pages.updateNews', ['currNews' => $currNews]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $currNews = ConfNew::find($id);
        
        $this->validate($request, [
			'label' => 'required|min:1|max:100',
			'text' => 'required|max:1024',
			'img' => 'required|max:255'
		]);

        //dd($currNews);
        
        $currNews->label = $request->label;
        $currNews->text = $request->text;
		$currNews->img = $request->img;
        
        $currNews->save();
        session()->flash('message', 'Текущая новость успешно обновлена!');
        
        return redirect()->route('ConfNews');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $currNews = ConfNew::find($id);

        $currNews->delete();
        session()->flash('message', 'Текущая новость успешно удалена!');
        
        return redirect()->route('ConfNews');
    }
}
