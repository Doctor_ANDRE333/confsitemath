<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class IsRedactor
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ((Auth::guest()) || (Auth::user()->role != 'redactor')){
            session()->flash('warning', 'Вы не являетесь редактором!');
            return redirect('/');
        }

        return $next($request);
    }
}
