<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    public function User(){
		return $this->belongsTo('App\User', 'user_id');
	}

    public function Comment(){
		return $this->belongsTo('App\Comment', 'comment_id');
	}
}
